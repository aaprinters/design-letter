<?php 
// direct access not accessable
if ( ! defined( 'ABSPATH' ) ) exit;  

class EXTENDONS_PRODUCT_BUNDLES_FRONT extends EXTENDONS_PRODUCT_BUNDLES {

    public $hide_bundle_items_in_cart = false;

	public function __construct() {

		// S C R I P T S    F O R     F R O N T - E N D
        add_action( 'wp_enqueue_scripts', array( $this, 'extendons_product_review_front_scripts' ) );

        // W O O C O M M E R C E      R E A D Y
		add_action( 'woocommerce_wooextb_add_to_cart', array( $this, 'ext_woocommerce_wooextb_add_to_cart' ) );
		add_filter( 'woocommerce_add_cart_item_data', array( $this, 'ext_woocommerce_add_cart_item_data' ), 10, 2 );
		add_action( 'woocommerce_add_to_cart', array( $this, 'ext_woocommerce_add_to_cart' ), 10, 6 );
        add_filter( 'woocommerce_cart_item_remove_link', array( $this, 'woocommerce_cart_item_remove_link' ), 10, 2 );
        add_filter( 'woocommerce_cart_item_quantity', array( $this, 'woocommerce_cart_item_quantity' ), 10, 2 );
        add_action( 'woocommerce_after_cart_item_quantity_update', array( $this, 'update_cart_item_quantity' ), 1, 2 );
        add_action( 'woocommerce_before_cart_item_quantity_zero', array( $this, 'update_cart_item_quantity' ), 1 );
        add_filter( 'woocommerce_cart_item_price', array( $this, 'woocommerce_cart_item_price' ), 99, 3 );
        add_filter( 'woocommerce_cart_item_subtotal', array( $this, 'bundles_item_subtotal' ), 99, 3 );
        add_filter( 'woocommerce_checkout_item_subtotal', array( $this, 'bundles_item_subtotal' ), 10, 3 );
        add_filter( 'woocommerce_add_cart_item', array( $this, 'woocommerce_add_cart_item' ), 10, 2 );
        add_action( 'woocommerce_cart_item_removed', array( $this, 'woocommerce_cart_item_removed' ), 10, 2 );
        add_action( 'woocommerce_cart_item_restored', array( $this, 'woocommerce_cart_item_restored' ), 10, 2 );
        add_filter( 'woocommerce_cart_contents_count', array( $this, 'woocommerce_cart_contents_count' ) );
        add_filter( 'woocommerce_cart_item_class', array( $this, 'table_item_class_bundle' ), 10, 2 );
        add_filter( 'woocommerce_get_cart_item_from_session', array( $this, 'woocommerce_get_cart_item_from_session' ), 10, 3 );

        // H I D E     P R O D U C T      F R O M     CART,MINI CART, CHECKOUT, ORDERS
        add_filter( 'woocommerce_cart_item_visible', array($this,'woocommerce_cart_item_visible'), 10, 2 );
        add_filter( 'woocommerce_checkout_cart_item_visible', array( $this, 'woocommerce_cart_item_visible' ), 10, 2 );
        add_filter( 'woocommerce_widget_cart_item_visible', array( $this, 'woocommerce_cart_item_visible', ), 10, 2 );
        add_filter( 'woocommerce_order_item_visible', array( $this, 'woocommerce_cart_item_visible' ), 10, 2 );
        add_filter( 'woocommerce_order_item_visible', array( $this, 'hide_hidden_bundled_items_in_orders' ), 10, 2 );

        // H I D E     T H U M B N A I L     F R O M     CART,MINI CART, CHECKOUT, ORDERS
        add_filter( 'woocommerce_cart_item_thumbnail', array( $this, 'woocommerce_cart_item_thumbnail' ), 10, 3 );

        // O R D E R S 
        add_filter( 'woocommerce_order_formatted_line_subtotal', array( $this, 'woocommerce_order_formatted_line_subtotal' ), 10, 3 );
        add_action( 'woocommerce_add_order_item_meta', array( $this, 'woocommerce_add_order_item_meta' ), 10, 3 );
        add_filter( 'woocommerce_order_item_class', array( $this, 'table_item_class_bundle' ), 10, 2 );

        // S E T     P R I C E
        add_filter( 'woocommerce_get_price_html', array( $this, 'woocommerce_get_price_html' ), 15, 2 );
        add_action( 'woocommerce_before_calculate_totals', array($this,'add_custom_price') );

        // M A K E    P R O D U C T     P U R C H A S E A B L E
        add_filter('woocommerce_is_purchasable', array($this,'preorder_is_purchasable'), 10, 2);


        add_filter( 'woocommerce_add_cart_item_data', array($this,'iconic_add_engraving_text_to_cart_item'), 10, 3 );

        add_filter( 'woocommerce_get_item_data', array($this, 'iconic_display_engraving_text_cart'), 10, 2 );

	}

    public function iconic_display_engraving_text_cart( $item_data, $cart_item ) {
       
        if(isset($cart_item['bundle_item_id'])){
            $pid= $cart_item['product_id'];
            $pdata= wc_get_product( $pid );
            if($pdata->is_type( 'variable' ) ){
                // print_r($cart_item);
                $available_variations = $pdata->get_available_variations();
                $loopnum= $cart_item['loopnum'];
                $data="";
                for($i=0;$i<$loopnum;$i++){
                    $data=$data.$cart_item['product_variation'.$i];
                    if($i != $loopnum-1){
                        $data= $data.' - ';
                    }
                }
                // echo $data;
               // print_r($available_variations);
                //have to work on getting exact names for extra variations :'(
                // $key= key($available_variations[0]['attributes']);
                // $val= $available_variations[0]['attributes'][$key];
                $item_data[] = array(
                    'key'     => __( 'Variation', 'iconic' ),
                    'value'   => wc_clean( $data ),
                    'display' => '',
                    ); 
            }
        }
        if ( empty( $cart_item['product_variation'] ) ) {
            return $item_data;
        }

        return $item_data;
    }

    public function iconic_add_engraving_text_to_cart_item( $cart_item_data, $product_id, $variation_id ) {
    
    $loopnum= filter_input( INPUT_POST, 'loopnum' );
    for ($i=0;$i<$loopnum;$i++){
        $engraving_text[$i] = filter_input( INPUT_POST, 'product_variation'.$i );
        $cart_item_data['product_variation'.$i] = $engraving_text[$i];
    }
    if ( !empty( $cart_item['product_variation'] ) ) {
            $cart_item_data['product_variation'] = filter_input( INPUT_POST, 'product_variation' );
        }
    $cart_item_data['loopnum'] = $loopnum;
 
//  echo '<pre>';
// print_r($cart_item_data);
// exit;
    return $cart_item_data;
    }
 


    // make product purchasable
    public function preorder_is_purchasable( $is_purchasable, $product ) {

       $per_item_pricing = get_post_meta( $product->get_id(), '_extbundle_per_item_pricing', true);

       $product_bundles = get_post_meta( $product->get_id(), '_wcpb_bundles_product', true);
       
       if($product->get_type() == 'wooextb') {

            if( $product->get_price() == 0 || $product->get_price() == '' ) {
        
                $is_purchasable = true;
            
            } else if(isset($per_item_pricing) && $per_item_pricing == 'yes') {

                $is_purchasable = true;

            } 

        } else {

            return $is_purchasable;
        } 

        return $is_purchasable;

    }

    // add custom product price at your on risk
    public function add_custom_price( $cart_object ) {

        foreach ( $cart_object->cart_contents as $key => $value ) {

                $product_bundles = get_post_meta( $value['product_id'], '_wcpb_bundles_product', true);

                $per_item_pricing = get_post_meta( $value['product_id'], '_extbundle_per_item_pricing', true);

                $product_bundles = unserialize($product_bundles);

                $parent_product_price = wc_get_product( $value['product_id'] );

            if ( !empty( $product_bundles ) ) {
            
                // if per item pricing and price was empty
                if(isset($per_item_pricing) && $per_item_pricing == 'yes') {

                    $product = wc_get_product( $value['product_id'] );

                    if($product->get_price() == 0 || $product->get_price() == '') {

                        $value['data']->set_price($value['per_item_enable']);

                    }

                } else if(isset($per_item_pricing) && $per_item_pricing == 'no') {

                   $product = wc_get_product( $value['product_id'] );

                    if($product->get_price() == 0 || $product->get_price() == '') {

                        $value['data']->set_price($parent_product_price->get_regular_price());
                    }

                } else {

                    $value['data']->set_price($parent_product_price->get_regular_price());
                }
                
            }
        }
    }

    // set product price as front end html
    public function woocommerce_get_price_html($price, $product ) {

        if ( $product->get_type() != 'wooextb' )
           
            return $price;

        $per_item_pricing = get_post_meta( $product->get_id(), '_extbundle_per_item_pricing', true);

        $product_bundles = get_post_meta( $product->get_id(), '_wcpb_bundles_product', true);
   
        $product_bundles = unserialize($product_bundles);

        $parent_product_price = wc_get_product( $product->get_id() );

        if ( !empty( $product_bundles ) ) {

            if(isset($per_item_pricing) && $per_item_pricing == 'yes') {

                if($product->get_price() == 0 || $product->get_price() == '') {

                    $price = wc_price( 0 ) . $product->get_price_suffix();

                    return $price;

                }

            } 
            
            if(isset($per_item_pricing) && $per_item_pricing == 'no') {

                if($product->get_price() == 0 || $product->get_price() == '') {

                    $price = wc_price( $parent_product_price->get_regular_price() ) . $product->get_price_suffix();

                    return $price;

                } 
            }

            return $price;

        }

    }


    // TEMPALTE OVERRIDE FOR BUNDLE PRODUCT (SINGLE PRODUCT PAGE)
	public function ext_woocommerce_wooextb_add_to_cart() {
       
        global $product;

        $id = $product->get_id();
            
        $product_bundles = get_post_meta($product->get_id(), '_wcpb_bundles_product', true);
        
        $bundles_array = unserialize($product_bundles);
        
        if(!empty($bundles_array)) {

            wc_get_template( 'single-product/add-to-cart/extendons-bundles.php', array(), '', product_bundle_template_path . '/' );
        }
    }

    // ADDINIG PRODUCT ITEM DATA TO CART
    public function ext_woocommerce_add_cart_item_data($cart_item_data, $product_id) {

        // per item pricing
        $peritem_bundle_price =  isset($_REQUEST['bundle_total_price']) ? $_REQUEST['bundle_total_price'] : "";
        // max quantity
        $maxquantity = isset($_REQUEST['pro_quantity_allow']) ? $_REQUEST['pro_quantity_allow'] : "";

        // echo "<pre>";
        // print_r($maxquantity);
        // exit;


        // variation names if exist
        $product_variation = isset($_REQUEST['product_variation']) ? $_REQUEST['product_variation'] : "";
        // loop num
        $loopnum = isset($_REQUEST['loopnum']) ? $_REQUEST['loopnum'] : "";
        // product variation name if possible 

        $product = wc_get_product( $product_id );

        if ( !$product || !$product->is_type( 'wooextb' ) )
            return $cart_item_data;

        $bundle_data = get_post_meta($product->get_id(), '_wcpb_bundles_product', true); 

        $bundle_data_arr = unserialize($bundle_data);

        if ( !!$bundle_data_arr ) {
            
           $bundle_addtocart_params = isset( $cart_item_data[ 'extendon_bundle_add_to_cart_params' ] ) ? $cart_item_data[ 'extendon_bundle_add_to_cart_params' ] : $_REQUEST;

            $cartbundles = array();

            // add user define quantity
            if(isset($maxquantity) && $maxquantity!= ''){
                foreach($maxquantity as $key => $qty) {
                    $column_key = (int)array_search( $key, array_column($bundle_data_arr, 'product_id')) + 1;
                    $bundle_data_arr[$column_key]['maximum_qty'] = $qty;
                }
            }
            
            foreach ( $bundle_data_arr as $bundle_item_id => $bundle_item ) {

                if(isset($bundle_item['max_product_quantity']) && $bundle_item['max_product_quantity'] > 1){
                    $bundle_item['product_quantity'] =  $bundle_item['maximum_qty'];
                }

                $product = wc_get_product($bundle_item['product_id']);

                $bundled_optional_checked = isset ( $bundle_addtocart_params[ 'optional-product-bundle-item_' . $bundle_item_id ] ) ? true : false;

                if (isset($bundle_item['optional_product']) && !$bundled_optional_checked) {
                    continue;
                }

                $id = $bundle_item['product_id'];
                $bundled_product_type = $product->get_type();
                $bundled_product_quantity = $bundle_item['product_quantity'];
                $bundled_item_hidden = isset( $bundle_item[ 'hide_product' ] ) ? $bundle_item[ 'hide_product' ] : 0;
                $bundled_item_hidden_thumbnail = isset( $bundle_item[ 'hide_thumbnail' ] ) ? $bundle_item[ 'hide_thumbnail' ] : 0;

                $cartbundles[ $bundle_item_id ][ 'product_id' ] = $id;
                $cartbundles[ $bundle_item_id ][ 'type'] = $bundled_product_type;
                $cartbundles[ $bundle_item_id ][ 'quantity' ] = $bundled_product_quantity;
                $cartbundles[ $bundle_item_id ][ 'hidden' ] = $bundled_item_hidden;
                $cartbundles[ $bundle_item_id ][ 'hide_thumbnail' ] = $bundled_item_hidden_thumbnail;

            }

            $cart_item_data[ 'cartbundles' ]     = $cartbundles;
            $cart_item_data[ 'per_item_enable' ] = $peritem_bundle_price;
            $cart_item_data[ 'extbundled_items' ] = array();

        }

        return $cart_item_data;

    }

    // ADDING PRODUCT PRODUCTS
    public function ext_woocommerce_add_to_cart($cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data) {

        // echo "<pre>";
        // print_r($cart_item_data);
        // exit;

        if(isset($cart_item_data[ 'cartbundles' ] )) {

    		$bundled_items_cart_data = array( 'bundled_by' => $cart_item_key );

    		foreach ( $cart_item_data[ 'cartbundles' ] as $bundle_item_id => $bundle_item_cartstamp ) {

    			// BUNDALED BY
                $bundled_item_cart_data = $bundled_items_cart_data;
                // AUTO INCREMENT IDS
                $bundled_item_cart_data[ 'bundle_item_id' ] = $bundle_item_id;
                $bundled_item_cart_data[ 'ext_hidden_item'] = $bundle_item_cartstamp['hidden'];
                $bundled_item_cart_data[ 'ext_hidden_item_thumbnail'] = $bundle_item_cartstamp['hide_thumbnail'];
                // PRODUCT QUANTITIES
                $item_quantity = $bundle_item_cartstamp[ 'quantity' ];
                // MULTYPY THE QUANTITY WITH PARENT PRODUCT QUANTITY
                $total_quantity = $item_quantity * $quantity;
                // PRODUCTS IDS
                $prod_ids = $bundle_item_cartstamp[ 'product_id' ];

                $bundled_item_cart_key = $this->extendons_bundled_add_to_cart( $product_id, $prod_ids, $total_quantity, $variation_id, '', $bundled_item_cart_data );

                if ( $bundled_item_cart_key && !in_array( $bundled_item_cart_key, WC()->cart->cart_contents[ $cart_item_key ][ 'extbundled_items' ] ) ) {
                    
                    WC()->cart->cart_contents[ $cart_item_key ][ 'extbundled_items' ][] = $bundled_item_cart_key;
                    WC()->cart->cart_contents[ $cart_item_key ][ 'woextpb_parent' ]     = $cart_item_key;
                }

    		}

    	}

    }

    // CALLING FUNCTION TO CARD ABOVE
    public function extendons_bundled_add_to_cart( $bundle_parent_id, $bundle_product_ids, $quantity = 1, $variation_id = '', $variation = '', $cart_item_data ) {

        if ( $quantity <= 0 )
            return false;

        // CART DATA BUNDLE AND PARENT PRODUCT ARRAY
        $cart_item_data = ( array ) apply_filters( 'woocommerce_add_cart_item_data', $cart_item_data, $bundle_product_ids, $variation_id );

        // CART ID UNIQUENESS
        $cart_id = WC()->cart->generate_cart_id( $bundle_product_ids, $variation_id, $variation, $cart_item_data );
        // FIND KEY IN CART
        $cart_item_key = WC()->cart->find_product_in_cart( $cart_id );

        if ( 'product_variation' == get_post_type( $bundle_product_ids ) ) {
            $variation_id = $bundle_product_ids;
            $bundle_product_ids  = wp_get_post_parent_id( $variation_id );
        }

        $product_data = wc_get_product( $variation_id ? $variation_id : $bundle_product_ids );

        if ( !$cart_item_key ) {
            $cart_item_key = $cart_id;
            WC()->cart->cart_contents[ $cart_item_key ] = apply_filters( 'woocommerce_add_cart_item', array_merge( $cart_item_data, array(
                'product_id'   => $bundle_product_ids,
                'variation_id' => $variation_id,
                'variation'    => $variation,
                'quantity'     => $quantity,
                'data'         => $product_data
            ) ), $cart_item_key );
        }

        return $cart_item_key;
    }
    
    // REMOVING BUNDLED PRODUCT LINK IF BUNDLED PRODUCT
    public function woocommerce_cart_item_remove_link( $link, $cart_item_key ) {

        if ( isset( WC()->cart->cart_contents[ $cart_item_key ][ 'bundled_by' ] ) ) {
            $bundle_cart_key = WC()->cart->cart_contents[ $cart_item_key ][ 'bundled_by' ];
            if ( isset( WC()->cart->cart_contents[ $bundle_cart_key ] ) ) {
                return '';
            }
        }
        return $link;
    } 

    // BUNDLED PRODUCT QUANTITY
    public function woocommerce_cart_item_quantity( $quantity, $cart_item_key ) {

        if ( isset( WC()->cart->cart_contents[ $cart_item_key ][ 'bundled_by' ] ) ) {
            return WC()->cart->cart_contents[ $cart_item_key ][ 'quantity' ];
        }

        return $quantity;
    }

    // UPDATE BUNDLED PRODUCT QUANTITY
    public function update_cart_item_quantity( $cart_item_key, $quantity = 0 ) {

        if ( !empty( WC()->cart->cart_contents[ $cart_item_key ] ) ) {

            if ( $quantity <= 0 ) {
                $quantity = 0;
            } else {
                $quantity = WC()->cart->cart_contents[ $cart_item_key ][ 'quantity' ];
            }

            if ( !empty( WC()->cart->cart_contents[ $cart_item_key ][ 'cartbundles' ] ) && !isset( WC()->cart->cart_contents[ $cart_item_key ][ 'bundled_by' ] ) ) {

                $stamp = WC()->cart->cart_contents[ $cart_item_key ][ 'cartbundles' ];
            
                foreach ( WC()->cart->cart_contents as $key => $value ) {

                    if ( isset( $value[ 'bundled_by' ] ) && $cart_item_key == $value[ 'bundled_by' ] ) {
                        $bundle_item_ids  = $value[ 'bundle_item_id' ];
                        $bundle_quantity = $stamp[ $bundle_item_ids ][ 'quantity' ];
                        WC()->cart->set_quantity( $key, $quantity * $bundle_quantity, false );
                    }
                }
            }
        }

    }

    // REMOVE BUNDLED PRODUCT PRICE FROM CART
    public function woocommerce_cart_item_price( $price, $cart_item, $cart_item_key ) {
        
        if ( isset( $cart_item[ 'bundled_by' ] ) ) {
            $bundle_cart_key = $cart_item[ 'bundled_by' ];
            if ( isset( WC()->cart->cart_contents[ $bundle_cart_key ] ) ) {
                return '';
            }

        }

        return $price;
    }

    // SET BUNDLED PRODUCT PRICE AT SUBTOTAL
    public function bundles_item_subtotal( $subtotal, $cart_item, $cart_item_key ) {
        
        if ( isset( $cart_item[ 'bundled_by' ] ) ) {
            $bundle_cart_key = $cart_item[ 'bundled_by' ];
            if ( isset( WC()->cart->cart_contents[ $bundle_cart_key ] ) ) {
                return '';
            }
        }
        if ( isset( $cart_item[ 'extbundled_items' ] ) ) {
            if ( $cart_item[ 'data' ]->get_price() == 0 ) {
                return '';
            }
        }

        return $subtotal;
    }

    // SET PRICE TO ZERO IN CART
    public function woocommerce_add_cart_item( $cart_item, $cart_key ) {

        $cart_contents = WC()->cart->cart_contents;

        if ( isset( $cart_item[ 'bundled_by' ] ) ) {
            $bundle_cart_key = $cart_item[ 'bundled_by' ];
            if ( isset( $cart_contents[ $bundle_cart_key ] ) ) {
                $parent = $cart_contents[ $bundle_cart_key ][ 'data' ];
                $bundle_item_id = $cart_item[ 'bundle_item_id' ];
                $cart_item[ 'data' ]->set_price(0);
            }
        }

        return $cart_item;

    }

    // REMOVE ALL PRODUCT PRICE IF BUNDLE EXIST
    public function woocommerce_cart_item_removed( $cart_item_key, $cart ) {
        if ( !empty( $cart->removed_cart_contents[ $cart_item_key ][ 'extbundled_items' ] ) ) {
            $bundled_item_cart_keys = $cart->removed_cart_contents[ $cart_item_key ][ 'extbundled_items' ];
            foreach ( $bundled_item_cart_keys as $bundled_item_cart_key ) {
                if ( !empty( $cart->cart_contents[ $bundled_item_cart_key ] ) ) {
                    $remove = $cart->cart_contents[ $bundled_item_cart_key ];
                    $cart->removed_cart_contents[ $bundled_item_cart_key ] = $remove;
                    unset( $cart->cart_contents[ $bundled_item_cart_key ] );
                    do_action( 'woocommerce_cart_item_removed', $bundled_item_cart_key, $cart );
                }
            }

        }
    }

    // UNDO PRODUCT ONCE REMOVED THE BUNDLED PRODUCT
    public function woocommerce_cart_item_restored( $cart_item_key, $cart ) {
        if ( !empty( $cart->cart_contents[ $cart_item_key ][ 'extbundled_items' ] ) ) {
            $bundled_item_cart_keys = $cart->cart_contents[ $cart_item_key ][ 'extbundled_items' ];
            foreach ( $bundled_item_cart_keys as $bundled_item_cart_key ) {
                $cart->restore_cart_item( $bundled_item_cart_key );
            }
        }
    }

    // REMOVE BUNDLED PRODUCT FROM COUNT
    public function woocommerce_cart_contents_count( $count ) {
        $cart_contents = WC()->cart->cart_contents;

        $bundled_items_count = 0;
        foreach ( $cart_contents as $cart_item_key => $cart_item ) {
            if ( !empty( $cart_item[ 'bundled_by' ] ) ) {
                $bundled_items_count += $cart_item[ 'quantity' ];
            }
        }

        return intval( $count - $bundled_items_count );
    }

    // ADDING CLASSES FOR BUNDLED ITEMS PARENT AND CHILD
    public function table_item_class_bundle( $classname, $cart_item ) {
        if ( isset( $cart_item[ 'bundled_by' ] ) )
            return $classname . ' extendons-pro-bundle-child'; elseif ( isset( $cart_item[ 'cartbundles' ] ) )
            return $classname . ' extendons-pro-bundle-parent';

        return $classname;
    }

    // SET SESSIONS FOR PRODUCT BUNDLES
    public function woocommerce_get_cart_item_from_session( $cart_item, $item_session_values, $cart_item_key ) {
        $cart_contents = !empty( WC()->cart ) ? WC()->cart->cart_contents : '';
        if ( isset( $item_session_values[ 'extbundled_items' ] ) && !empty( $item_session_values[ 'extbundled_items' ] ) )
            $cart_item[ 'extbundled_items' ] = $item_session_values[ 'extbundled_items' ];

        if ( isset( $item_session_values[ 'cartbundles' ] ) ) {
            $cart_item[ 'cartbundles' ] = $item_session_values[ 'cartbundles' ];
        }

        if ( isset( $item_session_values[ 'bundled_by' ] ) ) {
            $cart_item[ 'bundled_by' ]      = $item_session_values[ 'bundled_by' ];
            $cart_item[ 'bundle_item_id' ]  = $item_session_values[ 'bundle_item_id' ];
            $bundle_cart_key                = $cart_item[ 'bundled_by' ];

            if ( isset( $cart_contents[ $bundle_cart_key ] ) ) {
                $parent = $cart_contents[ $bundle_cart_key ][ 'data' ];
                $bundle_item_id = $cart_item[ 'bundle_item_id' ];
                $cart_item[ 'data' ]->set_price(0);
            }

        }

        return $cart_item;

    }
    

    // -------------------------------------------------------------------------------
    // make hidden item remove from cart, minicart, checkout and the order success page
    // -------------------------------------------------------------------------------


    // hide item in cart, minicart, checkout, and order
    public function woocommerce_cart_item_visible( $value, $cart_item ) {

        if ( !isset( $cart_item[ 'bundled_by' ] ) )
                return $value;

            if ( $this->hide_bundle_items_in_cart )
                return false;

            $hidden = isset( $cart_item[ 'ext_hidden_item' ] ) ? $cart_item[ 'ext_hidden_item' ] : 0;
            if ( $hidden )
                return false;

            return true;
    }

    // hide item from order
    public function hide_hidden_bundled_items_in_orders( $visible, $item ) {

        if ( isset( $item[ '_ext_hidden_item' ] ) && $item[ '_ext_hidden_item' ] == true )
            $visible = false;

        return $visible;
    }

    // -------------------------------------------------------------------------------
    // remove thumbnail of item through out all woocommerce process
    // -------------------------------------------------------------------------------


    // hide item thumbnail
    public function woocommerce_cart_item_thumbnail( $thumbnail, $cart_item, $cart_item_key ) {
        if ( !isset( $cart_item[ 'bundled_by' ] ) )
            return $thumbnail;

        $hide_thumbnail = isset( $cart_item[ 'ext_hidden_item_thumbnail' ] ) ? $cart_item[ 'ext_hidden_item_thumbnail' ] : 0;

        if ( $hide_thumbnail )
            return '';

        return $thumbnail;
    }

    // -------------------------------------------------------------------------------
    // order subtotal remove and also add the meta fro product bundle products
    // -------------------------------------------------------------------------------

    // remove subtotal from bundled product
    public function woocommerce_order_formatted_line_subtotal( $subtotal, $item, $order ) {
        if ( isset( $item[ 'bundled_by' ] ) )
            return '';

        return $subtotal;
    }

    // add order item fro bundle products
    public function woocommerce_add_order_item_meta( $item_id, $values, $cart_item_key ) {
         if ( isset( $values[ 'bundled_by' ] ) ) {
            wc_add_order_item_meta( $item_id, '_bundled_by', $values[ 'bundled_by' ] );
            $hidden = isset( $values[ 'ext_hidden_item' ] ) ? $values[ 'ext_hidden_item' ] : false;
            wc_add_order_item_meta( $item_id, '_ext_hidden_item', $hidden );
            if($values['loopnum'] == 1){
                        wc_add_order_item_meta( $item_id, 'product_variation', $values['product_variation0'] );
            }
            else{
                $loopnum= $values['loopnum'];
                for($i=0 ;$i<$loopnum;$i++){
                    $num=$i+1;

                    wc_add_order_item_meta( $item_id, 'product_variation-'.$num, $values['product_variation'.$i] );
                }
            }
        } else {
            if ( isset( $values[ 'cartbundles' ] ) ) {
                wc_add_order_item_meta( $item_id, '_cartbundles', $values[ 'cartbundles' ] );
            }
        }
    }



    // ----------------------------------------------------------------------//
    // ----------------------- Per item calcualtion -------------------------//
    //---------------------------------------------------------------------//
    public function get_max_ifperitem_price(){

        global $post;

        $product_bundles = get_post_meta( $post->ID, '_wcpb_bundles_product', true);

        $bundle_array = unserialize($product_bundles);

        if (!empty($bundle_array)) {
         
            $total_price = 0;
            foreach ($bundle_array as $item_data) {
            
                $WC_Product = new WC_Product($item_data['product_id']);
                
                $regular_prices = $WC_Product->get_regular_price();

                $total_price+= $regular_prices * $item_data['product_quantity'];

            }

            return $total_price;
        }
        
    }


    // ENQUEU THE SCRIPTS AND STYPES FOR FRONT END
	public function extendons_product_review_front_scripts() { 

		wp_enqueue_style( 'product-bundles-frontend_style', plugins_url( '/Styles/frontend.css', __FILE__ ), false );

        wp_enqueue_style( 'product-bundles-iso-css', plugins_url( '/Styles/pretty-checkbox.min.css', __FILE__ ), false );

        wp_enqueue_script( 'product-bundle-frontend-addtocart', plugins_url( '/Scripts/addtocart-frontend.js', __FILE__ ), false );

        wp_localize_script( 'product-bundle-frontend-addtocart', 'woopbf_vars', array(
                'woopbf_nonce' => wp_create_nonce( 'woopbf_nonce' ),
                'ajax_url' => admin_url( 'admin-ajax.php' )
            )
        );

	} 	

} new EXTENDONS_PRODUCT_BUNDLES_FRONT();