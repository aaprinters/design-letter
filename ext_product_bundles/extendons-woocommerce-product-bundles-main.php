<?php 

/*
Plugin Name:  Extendons Product Bundles
Plugin URI: http://extendons.com
Description: Woocommerce product bundles
Author: Extendons
Version: 1.0.6
Developed By: Extendons
Author URI: http://extendons.com/
Support: http://support@extendons.com
textdomain: product-bundles-extendons
License: GPL-2.0+
License URI: http://www.gnu.org/licenses/gpl-2.0.txt
*/

class EXTENDONS_PRODUCT_BUNDLES {

	public function __construct() {

		$this->module_constant();

		add_action('wp_loaded', array( $this, 'pro_bundles_main_scripts_loading'));

		add_action( 'plugins_loaded', array( $this, 'pro_bundles_load_textdomain' ), 100 );

		add_action( 'wp_ajax_mishagetposts', array($this,'render_get_posts_ajax_callback') );

		add_action( 'wp_ajax_get_bundle_products', array($this,'get_information_selected_products') );

		require_once( product_bundles_dir.'extendons-woocommerce-product-bundles-admin.php');
		
		require_once( product_bundles_dir.'extendons-woocommerce-product-bundles-front.php');
		
	}


	public function get_information_selected_products() {

		if ( ! isset( $_POST['woopb_nonce'] ) || ! wp_verify_nonce( $_POST['woopb_nonce'], 'woopb_nonce' ) ) {
			
			die( 'Permissions check failed' );
		
		} else  {

			$product = wc_get_product( $_POST['woopb_ids'] );

			$inc_id = $_POST['item_count'];

				echo '<div class="product-bundle-items" rel="'.$inc_id.'" id="ext-pro-bundle-item'.$inc_id.'">
			  		<h3>
			  		<strong>
				  			<a target="blank" onclick="save_bundle_product(event);" href="'.get_edit_post_link($product->get_id()).'" class="dashicons dashicons-welcome-view-site"></a>
				  			<span>'. $product->get_name() .' - #'. $product->get_id() .'</span>
				  		</strong>
				  		<button onclick="save_bundle_remove(event, '.$inc_id.');" type="button" class="save-product-bundles button">Remove</button>
				  		<input type="hidden" value="'.$product->get_id().'" name="_wcpb_bundles_product['.$inc_id.'][product_id]" />
				  		<input type="hidden" value="'.$inc_id.'" name="_wcpb_bundles_product['.$inc_id.'][product_item_id]">
				  		<input type="hidden" value="'.$product->price.'" name="_wcpb_bundles_product['.$inc_id.'][product_price]" />
				  	</h3>
				  	<div>
				    	<div class="bundles-form">

				    		<div class="col-md-8">
				    			<div class="form-group">
									<label class="search-wooextb-labelfld" for="">'. __( 'Hide Product', '' ) .'</label>
									<input onChange="echeckbox(this);" name="_wcpb_bundles_product['.$inc_id.'][hide_product]" type="checkbox" value="0"/>
									<span class="woocommerce-help-tip" data-tip="'. __( 'Check if you want to hide this product in bundle', '' ) .'"></span>
								</div>
								<div class="clearfix"></div>
								<div class="form-group">
									<label class="search-wooextb-labelfld" for="">'. __( 'Hide Thumbnail', '' ) .'</label>
									<input onChange="echeckbox(this);" name="_wcpb_bundles_product['.$inc_id.'][hide_thumbnail]" type="checkbox" value="0"/>
									<span class="woocommerce-help-tip" data-tip="'. __( 'Check if you want to hide thumbnail of this product in bundle', '' ) .'"></span>
								</div>
								<div class="clearfix"></div>
								<div class="form-group">
									<label class="search-wooextb-labelfld" for="">'. __( 'Optional', '' ) .'</label>
									<input onChange="echeckbox(this);" name="_wcpb_bundles_product['.$inc_id.'][optional_product]" type="checkbox" value="0"/>
									<span class="woocommerce-help-tip" data-tip="'. __( 'Check if you want to make optional this product in bundle', '' ) .'"></span>
								</div>
								<div class="clearfix"></div>
								<div class="form-group">
									<label class="search-wooextb-labelfld" for="">'. __( 'Quantity', '' ) .'</label>
									<input value="1" name="_wcpb_bundles_product['.$inc_id.'][product_quantity]" type="number" class="form-control" min="1"/>
									<span class="woocommerce-help-tip" data-tip="'. __( 'Set quantity of this product', '' ) .'"></span>
								</div>
								<div class="clearfix"></div>
								<div class="form-group">
									<label class="search-wooextb-labelfld" for="">'. __( 'Max Quantity Allowed', '' ) .'</label>
									<input value="1" name="_wcpb_bundles_product['.$inc_id.'][max_product_quantity]" type="number" class="form-control" min="1"/>
									<span class="woocommerce-help-tip" data-tip="'. __( 'Enter maximum quantity allowed for this product', '' ) .'"></span>
								</div>
								<div class="clearfix"></div>
								<div class="form-group">
									<label class="search-wooextb-labelfld" for="">'. __( 'Product Title', '' ) .'</label>
									<input name="_wcpb_bundles_product['.$inc_id.'][product_title]" type="text" class="form-control" value="'.$product->get_title().'"/>
									<span class="woocommerce-help-tip" data-tip="'. __( 'Put Title of this product', '' ) .'"></span>
								</div>
								<div class="clearfix"></div>
								<div class="form-group">
									<label class="search-wooextb-labelfld" for="">'. __( 'Product Description', '' ) .'</label>
									<textarea name="_wcpb_bundles_product['.$inc_id.'][product_description]" class="form-control" rows="6" col="4">'.$product->get_short_description().'</textarea>
									<span class="woocommerce-help-tip" data-tip="'. __( 'Put Description of this product', '' ) .'"></span>
								</div>
							</div>

				    	</div>
				  	</div>
			  	</div>';
		
		} die();

	}

	// populate the dropdown of search
	public function render_get_posts_ajax_callback(){
 
		// we will pass post IDs and titles to this array
		$return = array();
	 
		$search_results = new WP_Query( array( 
			's'=> $_GET['q'],
			'post_status' => 'publish',
			'ignore_sticky_posts' => 1,
			'posts_per_page' => 50,
			'post_type' => array( 'product' )
		) );
		
		if( $search_results->have_posts() ) :
			
			while( $search_results->have_posts() ) : $search_results->the_post();	
				
				$title = ( mb_strlen( $search_results->post->post_title ) > 50 ) ? mb_substr( $search_results->post->post_title, 0, 49 ) . '...' : $search_results->post->post_title;
			
				$return[] = array( $search_results->post->ID, $title );

			endwhile;
		
		endif;
		
		echo json_encode( $return );
		
		die;
	}

	// module constant 
	public function module_constant() {

		if ( !defined( 'product_bundles_url' ) )
	    define( 'product_bundles_url', plugin_dir_url( __FILE__ ) );

	    if ( !defined( 'product_bundles_basename' ) )
	    define( 'product_bundles_basename', plugin_basename( __FILE__ ) );

	    if ( ! defined( 'product_bundles_dir' ) )
	    define( 'product_bundles_dir', plugin_dir_path( __FILE__ ) );

		if ( !defined( 'product_bundle_template_path' ) ) 
    	define( 'product_bundle_template_path', product_bundles_dir . 'templates' );

	}

	// loading text domain
	public function pro_bundles_load_textdomain() {
		
		load_plugin_textdomain( 'product-bundles-extendons', false, basename( plugin_basename( __FILE__ ) ) . '/languages/' );

	}

	//enqueue the scripts and css
	public function pro_bundles_main_scripts_loading() { 

		wp_enqueue_script( 'jquery' );	

		wp_enqueue_script( 'ext-jquery-ui-accordion', plugins_url( '/Scripts/jquery-ui.js',__FILE__ ), false);
	
		wp_enqueue_style( 'product-bundles-bakend_style', plugins_url( '/Styles/backend.css', __FILE__ ), false );

		wp_enqueue_style( 'product-bundles-bootstrap_style', plugins_url( '/Styles/bootstrap-iso.css', __FILE__ ), false );
		
		wp_enqueue_script( 'product-bundles-select-2.js', plugins_url( '/Scripts/select2.min.js',__FILE__ ), false);

		wp_enqueue_style( 'product-bundles-select-2.css', plugins_url( '/Styles/select2.min.css', __FILE__ ), false );

		wp_enqueue_script( 'product-bundles-accounting-js-woo', plugins_url( '/Scripts/accounting.min.js', __FILE__ ), false );
	} 	

} new EXTENDONS_PRODUCT_BUNDLES();
