<?php
/**
 * Template for Extendons Bundles
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

/** add to cart template */

global $product;

$currency = get_woocommerce_currency();
$string = get_woocommerce_currency_symbol( $currency );
$thousand_sap = wc_get_price_thousand_separator();
$deciamal_sap = wc_get_price_decimal_separator();

$bundle_data = get_post_meta($product->get_id(), '_wcpb_bundles_product', true); 
$peritempricing = get_post_meta($product->get_id(), '_extbundle_per_item_pricing', true); 
$parent_product_price = wc_get_product( $product->get_id() );

$bundle_data_array = unserialize($bundle_data);

if ( !empty( $bundle_data ) ) {

    $bundle_data = unserialize($bundle_data); ?>

    <?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

    <form class="cart" method="post" enctype='multipart/form-data'>
        <input type="hidden" name="bundle_total_price" id="total_price">
        <input type="hidden" name="parent_product_price" id="parent_product_price" value="<?php echo $parent_product_price->get_regular_price(); ?>">
        <div id="btnContainer">
            <button type="button" class="btn active" onclick="listView()">
                <i class="fa fa-bars"></i> <?php echo __('List', 'product-bundles-extendons'); ?>
            </button> 
            <button type="button" class="btn" onclick="gridView()">
                <i class="fa fa-th-large"></i> <?php echo __('Grid', 'product-bundles-extendons'); ?>
            </button>
        </div>

        <table>
            <tbody>

                <div class="bootstrap-iso">
                    <div class="row list-group">
							
                        <?php
						$letter_count=1;
						$icon_charm_count=1;
						$chain_length_count=1;
						$stickers_statements_count=1;
						$stone_charm_count=1;
						$enamel_charm_count=1;
						$stone_heart_charm_count=1;
						$ball_charm_count=1;
						$gift_box_count=1;
						foreach ( $bundle_data as $item_data ) { 

                            $WC_Product = new WC_Product($item_data['product_id']); 

                            if( !empty($WC_Product->get_regular_price()) ) {

                                $pro_bundle_price = $WC_Product->get_regular_price();

                            }else {

                                $pro_bundle_price = $item_data['product_price'];

                            } ?>

                            <?php if ( isset($item_data['hide_product']) )
                                continue; ?>
						<?php if($item_data['group_name'] == 'chain_length') { 
						
								if($chain_length_count == 1) {
									echo '<div class="bundle_product_heading"><h3>'.$item_data['group_name'].'</h3></div>';
								} 
								?>
								<div class="item col-xs-6 col-lg-6 column gridborder" style="width: 100%;">

                                    <div id="bundles_td">
                                        <div class="product-bundle-image <?php if(isset($item_data['hide_thumbnail']) && $item_data['hide_thumbnail'] == '1' ) { echo 'hide_thumbnail'; } ?>">
                                            <?php echo $WC_Product->get_image( array( 80, 80 )); ?>
                                        </div>
                                        <div class="product-bundle-price">
                                            <ins><?php echo wc_price($item_data['product_price']); ?></ins>
                                        </div>
                                        </div>
                                        <div id="bundles_td">
                                        <?php if(isset($item_data['max_product_quantity']) && $item_data['max_product_quantity'] > 1) { ?>
                                            <div class="product-bundle-name">
                                                <a href="<?php echo get_permalink($item_data['product_id']); ?>">
                                                    <span><?php echo $item_data['product_quantity']. ' x ' .$item_data['product_title']; ?></span>        
                                                </a>
                                            </div>
                                        <?php } else { ?>
                                             <div class="product-bundle-name">
                                                <a href="<?php echo get_permalink($item_data['product_id']); ?>">
                                                    <span><?php echo $item_data['product_quantity']. ' x ' .$item_data['product_title']; ?></span>        
                                                </a>
                                            </div>
                                        <?php } ?>
                                        <div class="product-description">
                                           <?php echo $item_data['product_description']; ?>
                                        </div>
                                        <div class="product-stock-bundles">
                                            <?php if($WC_Product->get_stock_quantity() > 0 ) { ?>
                                                <p><?php echo $WC_Product->get_stock_quantity().' In Stock'; ?></p>
                                            <?php } ?>
                                        </div>
                                        
                                        
                                        <?php $_product = wc_get_product($item_data['product_id'] );

                                        if($_product->is_type( 'variable' )) {

                                        $attribute_keys = array_keys( $_product->get_attributes() );
                                       // print_r($attribute_keys);
                                          $totaldd=sizeof($attribute_keys);
                                         
                                          ?>
                                           <input type="hidden" id="loopnum" name="loopnum" value="<?php echo $totaldd; ?>">
                                          <?php
                                          if($totaldd >1){
                                            for($i= 0; $i< $totaldd;$i++){
                                                $versionvalues = get_the_terms( $item_data['product_id'], $attribute_keys[$i] ); 
                                                // print_r($versionvalues);
                                                ?>
                                                <select name="product_variation<?php echo $i;?>" required>
                                                <option value="">Choose an option</option>
                                                <?php foreach ($versionvalues as $key => $value) { ?>
                                                <option value="<?php echo $value->name; ?>"><?php echo $value->name; ?></option>   
                                                <?php } ?>
                                                </select> <br><?php
                                            }
                                          }
                                          else{
                                            
                                            $a= wc_get_product($item_data['product_id']);
                                            $a= $a->get_attribute( $attribute_keys[0] );
                                            if(strpos($a,'|')){
                                                $versionvalues= explode('|', $a);
                                            }
                                            else{
                                               $versionvalues = wc_get_product_terms( $item_data['product_id'], $attribute_keys[0] ); 
                                            }
                                            ?>

                                            <select name="product_variation" required>
                                                <option value="">Choose an option</option>
                                                <?php foreach ($versionvalues as $key => $value) { ?>

                                                   <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                                    
                                                <?php } ?>

                                            </select> <?php
                                          }
                                        ?>
                                         <input data-price="<?php echo $pro_bundle_price; ?>" type="hidden" name="pro_quantity_allow[<?php echo $item_data['product_id']; ?>]" value="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>"  min="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>" max="<?php echo $item_data['max_product_quantity']; ?>" class="input-text qty-probundle" min="1">
                                        <?php } ?>
                                        
                                        <?php if(isset($item_data['optional_product'])) { ?>
											
												<div class="pretty p-switch product-bundles-optional">
													<input id="" class="icon_charm optional-product-bundle-item" type="checkbox" name="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>" id="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>" value="<?php echo $item_data['product_quantity'] * $pro_bundle_price ?>" required>
													<div class="state p-danger">
														<label for="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>"><?php echo __('', 'product-bundles-extendons'); ?></label>
													</div>
												</div>
											
										
										<?php } ?>
                                        <?php if(isset($item_data['max_product_quantity']) && $item_data['max_product_quantity'] > 1) { ?>
                                            <input data-price="<?php echo $pro_bundle_price; ?>" type="number" name="pro_quantity_allow[<?php echo $item_data['product_id']; ?>]" value="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>"  min="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>" max="<?php echo $item_data['max_product_quantity']; ?>" class="input-text qty-probundle" min="1">
                                        <?php } ?>
                                    </div>
						<?php 	
						$chain_length_count++;
						} 
						
						if($item_data['group_name'] == 'letter') { 
							if($letter_count == 1) {
									echo '<div class="bundle_product_heading"><h3>'.$item_data['group_name'].'</h3></div>';
								}
							?>
								<div class="item col-xs-6 col-lg-6 column gridborder" style="width: 100%;">
									<div id="bundles_td">
                                        <div class="product-bundle-image <?php if(isset($item_data['hide_thumbnail']) && $item_data['hide_thumbnail'] == '1' ) { echo 'hide_thumbnail'; } ?>">
                                            <?php echo $WC_Product->get_image( array( 80, 80 )); ?>
                                        </div>
                                        <div class="product-bundle-price">
                                            <ins><?php echo wc_price($item_data['product_price']); ?></ins>
                                        </div>
                                        </div>
                                        <div id="bundles_td">
                                        <?php if(isset($item_data['max_product_quantity']) && $item_data['max_product_quantity'] > 1) { ?>
                                            <div class="product-bundle-name">
                                                <a href="<?php echo get_permalink($item_data['product_id']); ?>">
                                                    <span><?php echo $item_data['product_quantity']. ' x ' .$item_data['product_title']; ?></span>        
                                                </a>
                                            </div>
                                        <?php } else { ?>
                                             <div class="product-bundle-name">
                                                <a href="<?php echo get_permalink($item_data['product_id']); ?>">
                                                    <span><?php echo $item_data['product_quantity']. ' x ' .$item_data['product_title']; ?></span>        
                                                </a>
                                            </div>
                                        <?php } ?>
                                        <div class="product-description">
                                           <?php echo $item_data['product_description']; ?>
                                        </div>
                                        <div class="product-stock-bundles">
                                            <?php if($WC_Product->get_stock_quantity() > 0 ) { ?>
                                                <p><?php echo $WC_Product->get_stock_quantity().' In Stock'; ?></p>
                                            <?php } ?>
                                        </div>
                                        
                                        
                                        <?php $_product = wc_get_product($item_data['product_id'] );

                                        if($_product->is_type( 'variable' )) {

                                        $attribute_keys = array_keys( $_product->get_attributes() );
                                       // print_r($attribute_keys);
                                          $totaldd=sizeof($attribute_keys);
                                         
                                          ?>
                                           <input type="hidden" id="loopnum" name="loopnum" value="<?php echo $totaldd; ?>">
                                          <?php
                                          if($totaldd >1){
                                            for($i= 0; $i< $totaldd;$i++){
                                                $versionvalues = get_the_terms( $item_data['product_id'], $attribute_keys[$i] ); 
                                                // print_r($versionvalues);
                                                ?>
                                                <select name="product_variation<?php echo $i;?>" required>
                                                <option value="">Choose an option</option>
                                                <?php foreach ($versionvalues as $key => $value) { ?>
                                                <option value="<?php echo $value->name; ?>"><?php echo $value->name; ?></option>   
                                                <?php } ?>
                                                </select> <br><?php
                                            }
                                          }
                                          else{
                                            
                                            $a= wc_get_product($item_data['product_id']);
                                            $a= $a->get_attribute( $attribute_keys[0] );
                                            if(strpos($a,'|')){
                                                $versionvalues= explode('|', $a);
                                            }
                                            else{
                                               $versionvalues = wc_get_product_terms( $item_data['product_id'], $attribute_keys[0] ); 
                                            }
                                            ?>

                                            <select name="product_variation" required>
                                                <option value="">Choose an option</option>
                                                <?php foreach ($versionvalues as $key => $value) { ?>

                                                   <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                                    
                                                <?php } ?>

                                            </select> <?php
                                          }
                                        ?>
                                         <input data-price="<?php echo $pro_bundle_price; ?>" type="hidden" name="pro_quantity_allow[<?php echo $item_data['product_id']; ?>]" value="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>"  min="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>" max="<?php echo $item_data['max_product_quantity']; ?>" class="input-text qty-probundle" min="1">
                                        <?php } ?>
                                        
                                        <?php if(isset($item_data['optional_product'])) { ?>
											
												<div class="pretty p-switch product-bundles-optional">
													<input id="" class="chain_length optional-product-bundle-item" type="checkbox" name="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>" id="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>" value="<?php echo $item_data['product_quantity'] * $pro_bundle_price ?>" required>
													<div class="state p-danger">
														<label for="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>"><?php echo __('', 'product-bundles-extendons'); ?></label>
													</div>
												</div>
											
										
										<?php } ?>
                                        <?php if(isset($item_data['max_product_quantity']) && $item_data['max_product_quantity'] > 1) { ?>
                                            <input data-price="<?php echo $pro_bundle_price; ?>" type="number" name="pro_quantity_allow[<?php echo $item_data['product_id']; ?>]" value="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>"  min="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>" max="<?php echo $item_data['max_product_quantity']; ?>" class="input-text qty-probundle" min="1">
                                        <?php } ?>
                                    </div>
						<?php 	
						$letter_count ++;
						} 
						if($item_data['group_name'] == 'icon_charm') { 
							if($icon_charm_count == 1) {
									echo '<div class="bundle_product_heading"><h3>'.$item_data['group_name'].'</h3></div>';
								}
							?>
								<div class="item col-xs-6 col-lg-6 column gridborder" style="width: 100%;">
									<div id="bundles_td">
                                        <div class="product-bundle-image <?php if(isset($item_data['hide_thumbnail']) && $item_data['hide_thumbnail'] == '1' ) { echo 'hide_thumbnail'; } ?>">
                                            <?php echo $WC_Product->get_image( array( 80, 80 )); ?>
                                        </div>
                                        <div class="product-bundle-price">
                                            <ins><?php echo wc_price($item_data['product_price']); ?></ins>
                                        </div>
                                        </div>
                                        <div id="bundles_td">
                                        <?php if(isset($item_data['max_product_quantity']) && $item_data['max_product_quantity'] > 1) { ?>
                                            <div class="product-bundle-name">
                                                <a href="<?php echo get_permalink($item_data['product_id']); ?>">
                                                    <span><?php echo $item_data['product_quantity']. ' x ' .$item_data['product_title']; ?></span>        
                                                </a>
                                            </div>
                                        <?php } else { ?>
                                             <div class="product-bundle-name">
                                                <a href="<?php echo get_permalink($item_data['product_id']); ?>">
                                                    <span><?php echo $item_data['product_quantity']. ' x ' .$item_data['product_title']; ?></span>        
                                                </a>
                                            </div>
                                        <?php } ?>
                                        <div class="product-description">
                                           <?php echo $item_data['product_description']; ?>
                                        </div>
                                        <div class="product-stock-bundles">
                                            <?php if($WC_Product->get_stock_quantity() > 0 ) { ?>
                                                <p><?php echo $WC_Product->get_stock_quantity().' In Stock'; ?></p>
                                            <?php } ?>
                                        </div>
                                        
                                        
                                        <?php $_product = wc_get_product($item_data['product_id'] );

                                        if($_product->is_type( 'variable' )) {

                                        $attribute_keys = array_keys( $_product->get_attributes() );
                                       // print_r($attribute_keys);
                                          $totaldd=sizeof($attribute_keys);
                                         
                                          ?>
                                           <input type="hidden" id="loopnum" name="loopnum" value="<?php echo $totaldd; ?>">
                                          <?php
                                          if($totaldd >1){
                                            for($i= 0; $i< $totaldd;$i++){
                                                $versionvalues = get_the_terms( $item_data['product_id'], $attribute_keys[$i] ); 
                                                // print_r($versionvalues);
                                                ?>
                                                <select name="product_variation<?php echo $i;?>" required>
                                                <option value="">Choose an option</option>
                                                <?php foreach ($versionvalues as $key => $value) { ?>
                                                <option value="<?php echo $value->name; ?>"><?php echo $value->name; ?></option>   
                                                <?php } ?>
                                                </select> <br><?php
                                            }
                                          }
                                          else{
                                            
                                            $a= wc_get_product($item_data['product_id']);
                                            $a= $a->get_attribute( $attribute_keys[0] );
                                            if(strpos($a,'|')){
                                                $versionvalues= explode('|', $a);
                                            }
                                            else{
                                               $versionvalues = wc_get_product_terms( $item_data['product_id'], $attribute_keys[0] ); 
                                            }
                                            ?>

                                            <select name="product_variation" required>
                                                <option value="">Choose an option</option>
                                                <?php foreach ($versionvalues as $key => $value) { ?>

                                                   <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                                    
                                                <?php } ?>

                                            </select> <?php
                                          }
                                        ?>
                                         <input data-price="<?php echo $pro_bundle_price; ?>" type="hidden" name="pro_quantity_allow[<?php echo $item_data['product_id']; ?>]" value="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>"  min="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>" max="<?php echo $item_data['max_product_quantity']; ?>" class="input-text qty-probundle" min="1">
                                        <?php } ?>
                                        
                                        <?php if(isset($item_data['optional_product'])) { ?>
											
												<div class="pretty p-switch product-bundles-optional">
													<input id="" class="icon_charm optional-product-bundle-item" type="checkbox" name="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>" id="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>" value="<?php echo $item_data['product_quantity'] * $pro_bundle_price ?>" required>
													<div class="state p-danger">
														<label for="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>"><?php echo __('', 'product-bundles-extendons'); ?></label>
													</div>
												</div>
											
										
										<?php } ?>
                                        <?php if(isset($item_data['max_product_quantity']) && $item_data['max_product_quantity'] > 1) { ?>
                                            <input data-price="<?php echo $pro_bundle_price; ?>" type="number" name="pro_quantity_allow[<?php echo $item_data['product_id']; ?>]" value="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>"  min="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>" max="<?php echo $item_data['max_product_quantity']; ?>" class="input-text qty-probundle" min="1">
                                        <?php } ?>
                                    </div>
						<?php 	
						$icon_charm_count ++;
						}
						if($item_data['group_name'] == 'stone_charm') { 
							if($stone_charm_count == 1) {
									echo '<div class="bundle_product_heading"><h3>'.$item_data['group_name'].'</h3></div>';
								}
							?>
								<div class="item col-xs-6 col-lg-6 column gridborder" style="width: 100%;">
									<div id="bundles_td">
                                        <div class="product-bundle-image <?php if(isset($item_data['hide_thumbnail']) && $item_data['hide_thumbnail'] == '1' ) { echo 'hide_thumbnail'; } ?>">
                                            <?php echo $WC_Product->get_image( array( 80, 80 )); ?>
                                        </div>
                                        <div class="product-bundle-price">
                                            <ins><?php echo wc_price($item_data['product_price']); ?></ins>
                                        </div>
                                        </div>
                                        <div id="bundles_td">
                                        <?php if(isset($item_data['max_product_quantity']) && $item_data['max_product_quantity'] > 1) { ?>
                                            <div class="product-bundle-name">
                                                <a href="<?php echo get_permalink($item_data['product_id']); ?>">
                                                    <span><?php echo $item_data['product_quantity']. ' x ' .$item_data['product_title']; ?></span>        
                                                </a>
                                            </div>
                                        <?php } else { ?>
                                             <div class="product-bundle-name">
                                                <a href="<?php echo get_permalink($item_data['product_id']); ?>">
                                                    <span><?php echo $item_data['product_quantity']. ' x ' .$item_data['product_title']; ?></span>        
                                                </a>
                                            </div>
                                        <?php } ?>
                                        <div class="product-description">
                                           <?php echo $item_data['product_description']; ?>
                                        </div>
                                        <div class="product-stock-bundles">
                                            <?php if($WC_Product->get_stock_quantity() > 0 ) { ?>
                                                <p><?php echo $WC_Product->get_stock_quantity().' In Stock'; ?></p>
                                            <?php } ?>
                                        </div>
                                        
                                        
                                        <?php $_product = wc_get_product($item_data['product_id'] );

                                        if($_product->is_type( 'variable' )) {

                                        $attribute_keys = array_keys( $_product->get_attributes() );
                                       // print_r($attribute_keys);
                                          $totaldd=sizeof($attribute_keys);
                                         
                                          ?>
                                           <input type="hidden" id="loopnum" name="loopnum" value="<?php echo $totaldd; ?>">
                                          <?php
                                          if($totaldd >1){
                                            for($i= 0; $i< $totaldd;$i++){
                                                $versionvalues = get_the_terms( $item_data['product_id'], $attribute_keys[$i] ); 
                                                // print_r($versionvalues);
                                                ?>
                                                <select name="product_variation<?php echo $i;?>" required>
                                                <option value="">Choose an option</option>
                                                <?php foreach ($versionvalues as $key => $value) { ?>
                                                <option value="<?php echo $value->name; ?>"><?php echo $value->name; ?></option>   
                                                <?php } ?>
                                                </select> <br><?php
                                            }
                                          }
                                          else{
                                            
                                            $a= wc_get_product($item_data['product_id']);
                                            $a= $a->get_attribute( $attribute_keys[0] );
                                            if(strpos($a,'|')){
                                                $versionvalues= explode('|', $a);
                                            }
                                            else{
                                               $versionvalues = wc_get_product_terms( $item_data['product_id'], $attribute_keys[0] ); 
                                            }
                                            ?>

                                            <select name="product_variation" required>
                                                <option value="">Choose an option</option>
                                                <?php foreach ($versionvalues as $key => $value) { ?>

                                                   <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                                    
                                                <?php } ?>

                                            </select> <?php
                                          }
                                        ?>
                                         <input data-price="<?php echo $pro_bundle_price; ?>" type="hidden" name="pro_quantity_allow[<?php echo $item_data['product_id']; ?>]" value="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>"  min="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>" max="<?php echo $item_data['max_product_quantity']; ?>" class="input-text qty-probundle" min="1">
                                        <?php } ?>
                                        
                                        <?php if(isset($item_data['optional_product'])) { ?>
											
												<div class="pretty p-switch product-bundles-optional">
													<input id="" class="stone_charm optional-product-bundle-item" type="checkbox" name="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>" id="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>" value="<?php echo $item_data['product_quantity'] * $pro_bundle_price ?>" required>
													<div class="state p-danger">
														<label for="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>"><?php echo __('', 'product-bundles-extendons'); ?></label>
													</div>
												</div>
											
										
										<?php } ?>
                                        <?php if(isset($item_data['max_product_quantity']) && $item_data['max_product_quantity'] > 1) { ?>
                                            <input data-price="<?php echo $pro_bundle_price; ?>" type="number" name="pro_quantity_allow[<?php echo $item_data['product_id']; ?>]" value="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>"  min="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>" max="<?php echo $item_data['max_product_quantity']; ?>" class="input-text qty-probundle" min="1">
                                        <?php } ?>
                                    </div>
						<?php 	
						$stone_charm_count ++;
						}
						if($item_data['group_name'] == 'enamel_charm') { 
							if($enamel_charm_count == 1) {
									echo '<div class="bundle_product_heading"><h3>'.$item_data['group_name'].'</h3></div>';
								}
							?>
								<div class="item col-xs-6 col-lg-6 column gridborder" style="width: 100%;">
									<div id="bundles_td">
                                        <div class="product-bundle-image <?php if(isset($item_data['hide_thumbnail']) && $item_data['hide_thumbnail'] == '1' ) { echo 'hide_thumbnail'; } ?>">
                                            <?php echo $WC_Product->get_image( array( 80, 80 )); ?>
                                        </div>
                                        <div class="product-bundle-price">
                                            <ins><?php echo wc_price($item_data['product_price']); ?></ins>
                                        </div>
                                        </div>
                                        <div id="bundles_td">
                                        <?php if(isset($item_data['max_product_quantity']) && $item_data['max_product_quantity'] > 1) { ?>
                                            <div class="product-bundle-name">
                                                <a href="<?php echo get_permalink($item_data['product_id']); ?>">
                                                    <span><?php echo $item_data['product_quantity']. ' x ' .$item_data['product_title']; ?></span>        
                                                </a>
                                            </div>
                                        <?php } else { ?>
                                             <div class="product-bundle-name">
                                                <a href="<?php echo get_permalink($item_data['product_id']); ?>">
                                                    <span><?php echo $item_data['product_quantity']. ' x ' .$item_data['product_title']; ?></span>        
                                                </a>
                                            </div>
                                        <?php } ?>
                                        <div class="product-description">
                                           <?php echo $item_data['product_description']; ?>
                                        </div>
                                        <div class="product-stock-bundles">
                                            <?php if($WC_Product->get_stock_quantity() > 0 ) { ?>
                                                <p><?php echo $WC_Product->get_stock_quantity().' In Stock'; ?></p>
                                            <?php } ?>
                                        </div>
                                        
                                        
                                        <?php $_product = wc_get_product($item_data['product_id'] );

                                        if($_product->is_type( 'variable' )) {

                                        $attribute_keys = array_keys( $_product->get_attributes() );
                                       // print_r($attribute_keys);
                                          $totaldd=sizeof($attribute_keys);
                                         
                                          ?>
                                           <input type="hidden" id="loopnum" name="loopnum" value="<?php echo $totaldd; ?>">
                                          <?php
                                          if($totaldd >1){
                                            for($i= 0; $i< $totaldd;$i++){
                                                $versionvalues = get_the_terms( $item_data['product_id'], $attribute_keys[$i] ); 
                                                // print_r($versionvalues);
                                                ?>
                                                <select name="product_variation<?php echo $i;?>" required>
                                                <option value="">Choose an option</option>
                                                <?php foreach ($versionvalues as $key => $value) { ?>
                                                <option value="<?php echo $value->name; ?>"><?php echo $value->name; ?></option>   
                                                <?php } ?>
                                                </select> <br><?php
                                            }
                                          }
                                          else{
                                            
                                            $a= wc_get_product($item_data['product_id']);
                                            $a= $a->get_attribute( $attribute_keys[0] );
                                            if(strpos($a,'|')){
                                                $versionvalues= explode('|', $a);
                                            }
                                            else{
                                               $versionvalues = wc_get_product_terms( $item_data['product_id'], $attribute_keys[0] ); 
                                            }
                                            ?>

                                            <select name="product_variation" required>
                                                <option value="">Choose an option</option>
                                                <?php foreach ($versionvalues as $key => $value) { ?>

                                                   <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                                    
                                                <?php } ?>

                                            </select> <?php
                                          }
                                        ?>
                                         <input data-price="<?php echo $pro_bundle_price; ?>" type="hidden" name="pro_quantity_allow[<?php echo $item_data['product_id']; ?>]" value="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>"  min="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>" max="<?php echo $item_data['max_product_quantity']; ?>" class="input-text qty-probundle" min="1">
                                        <?php } ?>
                                        
                                        <?php if(isset($item_data['optional_product'])) { ?>
											
												<div class="pretty p-switch product-bundles-optional">
													<input id="" class="enamel_charm optional-product-bundle-item" type="checkbox" name="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>" id="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>" value="<?php echo $item_data['product_quantity'] * $pro_bundle_price ?>" required>
													<div class="state p-danger">
														<label for="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>"><?php echo __('', 'product-bundles-extendons'); ?></label>
													</div>
												</div>
											
										
										<?php } ?>
                                        <?php if(isset($item_data['max_product_quantity']) && $item_data['max_product_quantity'] > 1) { ?>
                                            <input data-price="<?php echo $pro_bundle_price; ?>" type="number" name="pro_quantity_allow[<?php echo $item_data['product_id']; ?>]" value="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>"  min="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>" max="<?php echo $item_data['max_product_quantity']; ?>" class="input-text qty-probundle" min="1">
                                        <?php } ?>
                                    </div>
						<?php 	
						$enamel_charm_count ++;
						}
						
						if($item_data['group_name'] == 'ball_charm') { 
							if($ball_charm_count == 1) {
									echo '<div class="bundle_product_heading"><h3>'.$item_data['group_name'].'</h3></div>';
								}
							?>
								<div class="item col-xs-6 col-lg-6 column gridborder" style="width: 100%;">
									<div id="bundles_td">
                                        <div class="product-bundle-image <?php if(isset($item_data['hide_thumbnail']) && $item_data['hide_thumbnail'] == '1' ) { echo 'hide_thumbnail'; } ?>">
                                            <?php echo $WC_Product->get_image( array( 80, 80 )); ?>
                                        </div>
                                        <div class="product-bundle-price">
                                            <ins><?php echo wc_price($item_data['product_price']); ?></ins>
                                        </div>
                                        </div>
                                        <div id="bundles_td">
                                        <?php if(isset($item_data['max_product_quantity']) && $item_data['max_product_quantity'] > 1) { ?>
                                            <div class="product-bundle-name">
                                                <a href="<?php echo get_permalink($item_data['product_id']); ?>">
                                                    <span><?php echo $item_data['product_quantity']. ' x ' .$item_data['product_title']; ?></span>        
                                                </a>
                                            </div>
                                        <?php } else { ?>
                                             <div class="product-bundle-name">
                                                <a href="<?php echo get_permalink($item_data['product_id']); ?>">
                                                    <span><?php echo $item_data['product_quantity']. ' x ' .$item_data['product_title']; ?></span>        
                                                </a>
                                            </div>
                                        <?php } ?>
                                        <div class="product-description">
                                           <?php echo $item_data['product_description']; ?>
                                        </div>
                                        <div class="product-stock-bundles">
                                            <?php if($WC_Product->get_stock_quantity() > 0 ) { ?>
                                                <p><?php echo $WC_Product->get_stock_quantity().' In Stock'; ?></p>
                                            <?php } ?>
                                        </div>
                                        
                                        
                                        <?php $_product = wc_get_product($item_data['product_id'] );

                                        if($_product->is_type( 'variable' )) {

                                        $attribute_keys = array_keys( $_product->get_attributes() );
                                       // print_r($attribute_keys);
                                          $totaldd=sizeof($attribute_keys);
                                         
                                          ?>
                                           <input type="hidden" id="loopnum" name="loopnum" value="<?php echo $totaldd; ?>">
                                          <?php
                                          if($totaldd >1){
                                            for($i= 0; $i< $totaldd;$i++){
                                                $versionvalues = get_the_terms( $item_data['product_id'], $attribute_keys[$i] ); 
                                                // print_r($versionvalues);
                                                ?>
                                                <select name="product_variation<?php echo $i;?>" required>
                                                <option value="">Choose an option</option>
                                                <?php foreach ($versionvalues as $key => $value) { ?>
                                                <option value="<?php echo $value->name; ?>"><?php echo $value->name; ?></option>   
                                                <?php } ?>
                                                </select> <br><?php
                                            }
                                          }
                                          else{
                                            
                                            $a= wc_get_product($item_data['product_id']);
                                            $a= $a->get_attribute( $attribute_keys[0] );
                                            if(strpos($a,'|')){
                                                $versionvalues= explode('|', $a);
                                            }
                                            else{
                                               $versionvalues = wc_get_product_terms( $item_data['product_id'], $attribute_keys[0] ); 
                                            }
                                            ?>

                                            <select name="product_variation" required>
                                                <option value="">Choose an option</option>
                                                <?php foreach ($versionvalues as $key => $value) { ?>

                                                   <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                                    
                                                <?php } ?>

                                            </select> <?php
                                          }
                                        ?>
                                         <input data-price="<?php echo $pro_bundle_price; ?>" type="hidden" name="pro_quantity_allow[<?php echo $item_data['product_id']; ?>]" value="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>"  min="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>" max="<?php echo $item_data['max_product_quantity']; ?>" class="input-text qty-probundle" min="1">
                                        <?php } ?>
                                        
                                        <?php if(isset($item_data['optional_product'])) { ?>
											
												<div class="pretty p-switch product-bundles-optional">
													<input id="" class="ball_charm optional-product-bundle-item" type="checkbox" name="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>" id="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>" value="<?php echo $item_data['product_quantity'] * $pro_bundle_price ?>" required>
													<div class="state p-danger">
														<label for="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>"><?php echo __('', 'product-bundles-extendons'); ?></label>
													</div>
												</div>
											
										
										<?php } ?>
                                        <?php if(isset($item_data['max_product_quantity']) && $item_data['max_product_quantity'] > 1) { ?>
                                            <input data-price="<?php echo $pro_bundle_price; ?>" type="number" name="pro_quantity_allow[<?php echo $item_data['product_id']; ?>]" value="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>"  min="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>" max="<?php echo $item_data['max_product_quantity']; ?>" class="input-text qty-probundle" min="1">
                                        <?php } ?>
                                    </div>
						<?php 	
						$ball_charm_count ++;
						}
						
						if($item_data['group_name'] == 'stone_heart_charm') { 
							if($stone_heart_charm_count == 1) {
									echo '<div class="bundle_product_heading"><h3>'.$item_data['group_name'].'</h3></div>';
								}
							?>
								<div class="item col-xs-6 col-lg-6 column gridborder" style="width: 100%;">
									<div id="bundles_td">
                                        <div class="product-bundle-image <?php if(isset($item_data['hide_thumbnail']) && $item_data['hide_thumbnail'] == '1' ) { echo 'hide_thumbnail'; } ?>">
                                            <?php echo $WC_Product->get_image( array( 80, 80 )); ?>
                                        </div>
                                        <div class="product-bundle-price">
                                            <ins><?php echo wc_price($item_data['product_price']); ?></ins>
                                        </div>
                                        </div>
                                        <div id="bundles_td">
                                        <?php if(isset($item_data['max_product_quantity']) && $item_data['max_product_quantity'] > 1) { ?>
                                            <div class="product-bundle-name">
                                                <a href="<?php echo get_permalink($item_data['product_id']); ?>">
                                                    <span><?php echo $item_data['product_quantity']. ' x ' .$item_data['product_title']; ?></span>        
                                                </a>
                                            </div>
                                        <?php } else { ?>
                                             <div class="product-bundle-name">
                                                <a href="<?php echo get_permalink($item_data['product_id']); ?>">
                                                    <span><?php echo $item_data['product_quantity']. ' x ' .$item_data['product_title']; ?></span>        
                                                </a>
                                            </div>
                                        <?php } ?>
                                        <div class="product-description">
                                           <?php echo $item_data['product_description']; ?>
                                        </div>
                                        <div class="product-stock-bundles">
                                            <?php if($WC_Product->get_stock_quantity() > 0 ) { ?>
                                                <p><?php echo $WC_Product->get_stock_quantity().' In Stock'; ?></p>
                                            <?php } ?>
                                        </div>
                                        
                                        
                                        <?php $_product = wc_get_product($item_data['product_id'] );

                                        if($_product->is_type( 'variable' )) {

                                        $attribute_keys = array_keys( $_product->get_attributes() );
                                       // print_r($attribute_keys);
                                          $totaldd=sizeof($attribute_keys);
                                         
                                          ?>
                                           <input type="hidden" id="loopnum" name="loopnum" value="<?php echo $totaldd; ?>">
                                          <?php
                                          if($totaldd >1){
                                            for($i= 0; $i< $totaldd;$i++){
                                                $versionvalues = get_the_terms( $item_data['product_id'], $attribute_keys[$i] ); 
                                                // print_r($versionvalues);
                                                ?>
                                                <select name="product_variation<?php echo $i;?>" required>
                                                <option value="">Choose an option</option>
                                                <?php foreach ($versionvalues as $key => $value) { ?>
                                                <option value="<?php echo $value->name; ?>"><?php echo $value->name; ?></option>   
                                                <?php } ?>
                                                </select> <br><?php
                                            }
                                          }
                                          else{
                                            
                                            $a= wc_get_product($item_data['product_id']);
                                            $a= $a->get_attribute( $attribute_keys[0] );
                                            if(strpos($a,'|')){
                                                $versionvalues= explode('|', $a);
                                            }
                                            else{
                                               $versionvalues = wc_get_product_terms( $item_data['product_id'], $attribute_keys[0] ); 
                                            }
                                            ?>

                                            <select name="product_variation" required>
                                                <option value="">Choose an option</option>
                                                <?php foreach ($versionvalues as $key => $value) { ?>

                                                   <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                                    
                                                <?php } ?>

                                            </select> <?php
                                          }
                                        ?>
                                         <input data-price="<?php echo $pro_bundle_price; ?>" type="hidden" name="pro_quantity_allow[<?php echo $item_data['product_id']; ?>]" value="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>"  min="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>" max="<?php echo $item_data['max_product_quantity']; ?>" class="input-text qty-probundle" min="1">
                                        <?php } ?>
                                        
                                        <?php if(isset($item_data['optional_product'])) { ?>
											
												<div class="pretty p-switch product-bundles-optional">
													<input id="" class="stone_heart_charm optional-product-bundle-item" type="checkbox" name="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>" id="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>" value="<?php echo $item_data['product_quantity'] * $pro_bundle_price ?>" required>
													<div class="state p-danger">
														<label for="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>"><?php echo __('', 'product-bundles-extendons'); ?></label>
													</div>
												</div>
											
										
										<?php } ?>
                                        <?php if(isset($item_data['max_product_quantity']) && $item_data['max_product_quantity'] > 1) { ?>
                                            <input data-price="<?php echo $pro_bundle_price; ?>" type="number" name="pro_quantity_allow[<?php echo $item_data['product_id']; ?>]" value="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>"  min="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>" max="<?php echo $item_data['max_product_quantity']; ?>" class="input-text qty-probundle" min="1">
                                        <?php } ?>
                                    </div>
						<?php 	
						$stone_heart_charm_count ++;
						}
						
						if($item_data['group_name'] == 'gift_box') { 
							if($gift_box_count == 1) {
									echo '<div class="bundle_product_heading"><h3>'.$item_data['group_name'].'</h3></div>';
								}
							?>
								<div class="item col-xs-6 col-lg-6 column gridborder" style="width: 100%;">
									<div id="bundles_td">
                                        <div class="product-bundle-image <?php if(isset($item_data['hide_thumbnail']) && $item_data['hide_thumbnail'] == '1' ) { echo 'hide_thumbnail'; } ?>">
                                            <?php echo $WC_Product->get_image( array( 80, 80 )); ?>
                                        </div>
                                        <div class="product-bundle-price">
                                            <ins><?php echo wc_price($item_data['product_price']); ?></ins>
                                        </div>
                                        </div>
                                        <div id="bundles_td">
                                        <?php if(isset($item_data['max_product_quantity']) && $item_data['max_product_quantity'] > 1) { ?>
                                            <div class="product-bundle-name">
                                                <a href="<?php echo get_permalink($item_data['product_id']); ?>">
                                                    <span><?php echo $item_data['product_quantity']. ' x ' .$item_data['product_title']; ?></span>        
                                                </a>
                                            </div>
                                        <?php } else { ?>
                                             <div class="product-bundle-name">
                                                <a href="<?php echo get_permalink($item_data['product_id']); ?>">
                                                    <span><?php echo $item_data['product_quantity']. ' x ' .$item_data['product_title']; ?></span>        
                                                </a>
                                            </div>
                                        <?php } ?>
                                        <div class="product-description">
                                           <?php echo $item_data['product_description']; ?>
                                        </div>
                                        <div class="product-stock-bundles">
                                            <?php if($WC_Product->get_stock_quantity() > 0 ) { ?>
                                                <p><?php echo $WC_Product->get_stock_quantity().' In Stock'; ?></p>
                                            <?php } ?>
                                        </div>
                                        
                                        
                                        <?php $_product = wc_get_product($item_data['product_id'] );

                                        if($_product->is_type( 'variable' )) {

                                        $attribute_keys = array_keys( $_product->get_attributes() );
                                       // print_r($attribute_keys);
                                          $totaldd=sizeof($attribute_keys);
                                         
                                          ?>
                                           <input type="hidden" id="loopnum" name="loopnum" value="<?php echo $totaldd; ?>">
                                          <?php
                                          if($totaldd >1){
                                            for($i= 0; $i< $totaldd;$i++){
                                                $versionvalues = get_the_terms( $item_data['product_id'], $attribute_keys[$i] ); 
                                                // print_r($versionvalues);
                                                ?>
                                                <select name="product_variation<?php echo $i;?>" required>
                                                <option value="">Choose an option</option>
                                                <?php foreach ($versionvalues as $key => $value) { ?>
                                                <option value="<?php echo $value->name; ?>"><?php echo $value->name; ?></option>   
                                                <?php } ?>
                                                </select> <br><?php
                                            }
                                          }
                                          else{
                                            
                                            $a= wc_get_product($item_data['product_id']);
                                            $a= $a->get_attribute( $attribute_keys[0] );
                                            if(strpos($a,'|')){
                                                $versionvalues= explode('|', $a);
                                            }
                                            else{
                                               $versionvalues = wc_get_product_terms( $item_data['product_id'], $attribute_keys[0] ); 
                                            }
                                            ?>

                                            <select name="product_variation" required>
                                                <option value="">Choose an option</option>
                                                <?php foreach ($versionvalues as $key => $value) { ?>

                                                   <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                                    
                                                <?php } ?>

                                            </select> <?php
                                          }
                                        ?>
                                         <input data-price="<?php echo $pro_bundle_price; ?>" type="hidden" name="pro_quantity_allow[<?php echo $item_data['product_id']; ?>]" value="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>"  min="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>" max="<?php echo $item_data['max_product_quantity']; ?>" class="input-text qty-probundle" min="1">
                                        <?php } ?>
                                        
                                        <?php if(isset($item_data['optional_product'])) { ?>
											
												<div class="pretty p-switch product-bundles-optional">
													<input id="" class="gift_box optional-product-bundle-item" type="checkbox" name="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>" id="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>" value="<?php echo $item_data['product_quantity'] * $pro_bundle_price ?>" required>
													<div class="state p-danger">
														<label for="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>"><?php echo __('', 'product-bundles-extendons'); ?></label>
													</div>
												</div>
											
										
										<?php } ?>
                                        <?php if(isset($item_data['max_product_quantity']) && $item_data['max_product_quantity'] > 1) { ?>
                                            <input data-price="<?php echo $pro_bundle_price; ?>" type="number" name="pro_quantity_allow[<?php echo $item_data['product_id']; ?>]" value="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>"  min="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>" max="<?php echo $item_data['max_product_quantity']; ?>" class="input-text qty-probundle" min="1">
                                        <?php } ?>
                                    </div>
						<?php 	
						$gift_box_count ++;
						}
						
						if($item_data['group_name'] == 'stickers_statements') { 
							if($stickers_statements_count == 1) {
									echo '<div class="bundle_product_heading"><h3>'.$item_data['group_name'].'</h3></div>';
								}
							?>
								<div class="item col-xs-6 col-lg-6 column gridborder" style="width: 100%;">
									<div id="bundles_td">
                                        <div class="product-bundle-image <?php if(isset($item_data['hide_thumbnail']) && $item_data['hide_thumbnail'] == '1' ) { echo 'hide_thumbnail'; } ?>">
                                            <?php echo $WC_Product->get_image( array( 80, 80 )); ?>
                                        </div>
                                        <div class="product-bundle-price">
                                            <ins><?php echo wc_price($item_data['product_price']); ?></ins>
                                        </div>
                                        </div>
                                        <div id="bundles_td">
                                        <?php if(isset($item_data['max_product_quantity']) && $item_data['max_product_quantity'] > 1) { ?>
                                            <div class="product-bundle-name">
                                                <a href="<?php echo get_permalink($item_data['product_id']); ?>">
                                                    <span><?php echo $item_data['product_quantity']. ' x ' .$item_data['product_title']; ?></span>        
                                                </a>
                                            </div>
                                        <?php } else { ?>
                                             <div class="product-bundle-name">
                                                <a href="<?php echo get_permalink($item_data['product_id']); ?>">
                                                    <span><?php echo $item_data['product_quantity']. ' x ' .$item_data['product_title']; ?></span>        
                                                </a>
                                            </div>
                                        <?php } ?>
                                        <div class="product-description">
                                           <?php echo $item_data['product_description']; ?>
                                        </div>
                                        <div class="product-stock-bundles">
                                            <?php if($WC_Product->get_stock_quantity() > 0 ) { ?>
                                                <p><?php echo $WC_Product->get_stock_quantity().' In Stock'; ?></p>
                                            <?php } ?>
                                        </div>
                                        
                                        
                                        <?php $_product = wc_get_product($item_data['product_id'] );

                                        if($_product->is_type( 'variable' )) {

                                        $attribute_keys = array_keys( $_product->get_attributes() );
                                       // print_r($attribute_keys);
                                          $totaldd=sizeof($attribute_keys);
                                         
                                          ?>
                                           <input type="hidden" id="loopnum" name="loopnum" value="<?php echo $totaldd; ?>">
                                          <?php
                                          if($totaldd >1){
                                            for($i= 0; $i< $totaldd;$i++){
                                                $versionvalues = get_the_terms( $item_data['product_id'], $attribute_keys[$i] ); 
                                                // print_r($versionvalues);
                                                ?>
                                                <select name="product_variation<?php echo $i;?>" required>
                                                <option value="">Choose an option</option>
                                                <?php foreach ($versionvalues as $key => $value) { ?>
                                                <option value="<?php echo $value->name; ?>"><?php echo $value->name; ?></option>   
                                                <?php } ?>
                                                </select> <br><?php
                                            }
                                          }
                                          else{
                                            
                                            $a= wc_get_product($item_data['product_id']);
                                            $a= $a->get_attribute( $attribute_keys[0] );
                                            if(strpos($a,'|')){
                                                $versionvalues= explode('|', $a);
                                            }
                                            else{
                                               $versionvalues = wc_get_product_terms( $item_data['product_id'], $attribute_keys[0] ); 
                                            }
                                            ?>

                                            <select name="product_variation" required>
                                                <option value="">Choose an option</option>
                                                <?php foreach ($versionvalues as $key => $value) { ?>

                                                   <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                                    
                                                <?php } ?>

                                            </select> <?php
                                          }
                                        ?>
                                         <input data-price="<?php echo $pro_bundle_price; ?>" type="hidden" name="pro_quantity_allow[<?php echo $item_data['product_id']; ?>]" value="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>"  min="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>" max="<?php echo $item_data['max_product_quantity']; ?>" class="input-text qty-probundle" min="1">
                                        <?php } ?>
                                        
                                        <?php if(isset($item_data['optional_product'])) { ?>
											
												<div class="pretty p-switch product-bundles-optional">
													<input id="" class="stickers_statements optional-product-bundle-item" type="checkbox" name="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>" id="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>" value="<?php echo $item_data['product_quantity'] * $pro_bundle_price ?>" required>
													<div class="state p-danger">
														<label for="optional-product-bundle-item_<?php echo $item_data['product_item_id']; ?>"><?php echo __('', 'product-bundles-extendons'); ?></label>
													</div>
												</div>
											
										
										<?php } ?>
                                        <?php if(isset($item_data['max_product_quantity']) && $item_data['max_product_quantity'] > 1) { ?>
                                            <input data-price="<?php echo $pro_bundle_price; ?>" type="number" name="pro_quantity_allow[<?php echo $item_data['product_id']; ?>]" value="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>"  min="<?php if(isset($item_data['product_quantity'])) {echo $item_data['product_quantity'];}
                                            else{
                                                echo 1;
                                            } ?>" max="<?php echo $item_data['max_product_quantity']; ?>" class="input-text qty-probundle" min="1">
                                        <?php } ?>
                                    </div>
						<?php 	
						$stickers_statements_count ++;
						} ?>
						
						
                                </div>
                                
                        <?php
						} ?>

                    </div><!-- row bootstrap starts from here  -->
                </div><!-- bootstrap start from here -->

            </tbody>
        </table>
            <script type="text/javascript">
                 /* jQuery(function () {
                    jQuery('.subject-list').on('change', function() {
						jQuery('.subject-list').not(this).prop('checked', false);  
					});
                });  */ 
            </script>
        <script type="text/javascript">    
                var elements = document.getElementsByClassName("column");
                var i;
                // List View
                function listView() {
                    for (i = 0; i < elements.length; i++) {
                        elements[i].style.width = "100%";
                        elements[i].classList.add("listborder");
                        elements[i].classList.remove("gridborder");
                    }
                }
                // Grid View
                function gridView() {
                    for (i = 0; i < elements.length; i++) {
                        elements[i].style.width = "50%";
                        elements[i].classList.remove("listborder");
                        elements[i].classList.add("gridborder");
                    }
                }
                // chaning the active on buttons (list grid)
                var container = document.getElementById("btnContainer");
                var btns = container.getElementsByClassName("btn");
                for (var i = 0; i < btns.length; i++) {
                    btns[i].addEventListener("click", function(){
                    var current = document.getElementsByClassName("active");
                    current[0].className = current[0].className.replace(" active", "");
                    this.className += " active";
                  });
                }

        </script>

        <?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

            <?php if ( !$product->is_sold_individually() )
            woocommerce_quantity_input( array(
                'min_value' => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
                'max_value' => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product ))
            ); ?>


            <input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>"/>

            <button type="submit" class="single_add_to_cart_button button alt"><?php echo $product->single_add_to_cart_text(); ?></button>

        <?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

    </form>        

    <?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

    <script type="text/javascript">

        jQuery( document ).ready(function() {
          //  jQuery('.single_add_to_cart_button').addClass('disabled');
          //   jQuery('.single_add_to_cart_button').attr('style=', "pointer-events: 'none' ");
          // // alert(  document.getElementsByName("product_variation"));
          // jQuery("[name='product_variation']").change(function(){
          //    var fields = jQuery("[name='product_variation']");
          // for(var i= 0;i<fields.length;i++ ){
          //   if(fields[i].value == 'Choose an option'){
          //       jQuery('.single_add_to_cart_button').addClass('disabled');
          //   }
          //   else{
          //        jQuery('.single_add_to_cart_button').removeClass('disabled');
          //   }
          //     }
          // });
         
           // console.log( value.length);
            // checking if per item pricing is enable
            var peritempricing = "<?php echo $peritempricing; ?>";
            
            if(peritempricing == "yes") {

                var json_array = JSON.parse('<?php echo json_encode($bundle_data);?>');

                var arr = [];

                for(var x in json_array){
                
                  arr.push(json_array[x]);
                
                }

                var price = 0;
                
                for (i = 0; i < arr.length; i++) {
                    
                    if(!arr[i]['optional_product'] && !arr[i]['optional_product'] == 1) {
            
                        price += parseInt(arr[i]['product_price'] * arr[i]['product_quantity']);
                    
                    } 

                }

                var sum = parseFloat(price);

                // if per item pricing is enable so need to add the additional addons values
                jQuery('.optional-product-bundle-item').on('change', function() {
                    
                    if(jQuery(this).is(':checked')) {
                        sum += parseInt(jQuery(this).val());

                    } else {
                        sum -= parseInt(jQuery(this).val());
                    }
                
                    var price_form = "<?php echo get_option( 'woocommerce_currency_pos' ); ?>";
                    
                    var op_price = "";

                    op_price = accounting.formatMoney(sum, "<?php echo $string; ?>", "<?php echo wc_get_price_decimals(); ?>", "<?php echo $thousand_sap; ?>", "<?php echo $deciamal_sap; ?>");

                    jQuery('.price').html(op_price);

                    jQuery('#total_price').val(sum);

                });

                var sum = parseFloat(sum);

                // if enable item quantity so need to set form here
                jQuery(".qty-probundle:input[type=number]").on('change keyup', function () {                   

                    var currentprice = jQuery(this).attr("data-price");
                    
                    var productqty = jQuery(this).val();

                    if(productqty) {
                        
                        sum1 = parseInt(currentprice) * parseInt(productqty);

                        sum1 += sum;
                        
                    }

                    var price_form = "<?php echo get_option( 'woocommerce_currency_pos' ); ?>";
                    
                    var op_price = "";

                    op_price = accounting.formatMoney(sum1, "<?php echo $string; ?>", "<?php echo wc_get_price_decimals(); ?>", "<?php echo $thousand_sap; ?>", "<?php echo $deciamal_sap; ?>");

                    jQuery('.price').html(op_price);

                    jQuery('#total_price').val(sum1);
                      
                });

                var price_form = "<?php echo get_option( 'woocommerce_currency_pos' ); ?>";
                
                var op_price = "";

                op_price = accounting.formatMoney(sum, "<?php echo $string; ?>", "<?php echo wc_get_price_decimals(); ?>", "<?php echo $thousand_sap; ?>", "<?php echo $deciamal_sap; ?>");

                jQuery('.price').html(op_price);

                jQuery('#total_price').val(sum);

            }
        });

    </script>
<?php }
