<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WC_Product_wooextb extends WC_Product {

	public function __construct( $product ) {

		$this->product_type = 'wooextb';

		parent::__construct( $product );

	}

}