// var woosbTimeout = null;

jQuery(document).ready(function (jQuery) {
    
    jQuery( "#wooext_selectd_bundle" ).accordion({heightStyle: "content", collapsible : true, active : false, header: '> div.product-bundle-items >h3' });


    var icons = jQuery( "#wooext_selectd_bundle" ).accordion( "option", "icons" );
        jQuery('.open-uiacc').click(function () {
        jQuery('.ui-accordion-header').removeClass('ui-corner-all').addClass('ui-accordion-header-active ui-state-active ui-corner-top').attr({
            'aria-selected': 'true',
            'tabindex': '0'
        });
        jQuery('.ui-accordion-header-icon').removeClass(icons.header).addClass(icons.headerSelected);
        jQuery('.ui-accordion-content').addClass('ui-accordion-content-active').attr({
            'aria-expanded': 'true',
            'aria-hidden': 'false'
        }).show();
        jQuery(this).attr("disabled","disabled");
        jQuery('.close-uiacc').removeAttr("disabled");
    });
    jQuery('.close-uiacc').click(function () {
        jQuery('.ui-accordion-header').removeClass('ui-accordion-header-active ui-state-active ui-corner-top').addClass('ui-corner-all').attr({
            'aria-selected': 'false',
            'tabindex': '-1'
        });
        jQuery('.ui-accordion-header-icon').removeClass(icons.headerSelected).addClass(icons.header);
        jQuery('.ui-accordion-content').removeClass('ui-accordion-content-active').attr({
            'aria-expanded': 'false',
            'aria-hidden': 'true'
        }).hide();
        jQuery(this).attr("disabled","disabled");
        jQuery('.open-uiacc').removeAttr("disabled");
    });
    jQuery('.ui-accordion-header').click(function () {
        jQuery('.open-uiacc').removeAttr("disabled");
        jQuery('.close-uiacc').removeAttr("disabled");
        
    });


    jQuery(document).ready( function() {
        jQuery('#wooext_selectd_bundle').accordion({
            collapsible:true,
            heightStyle: "content",
            beforeActivate: function(event, ui) {
                 // The accordion believes a panel is being opened
                if (ui.newHeader[0]) {
                    var currHeader  = ui.newHeader;
                    var currContent = currHeader.next('.ui-accordion-content');
                 // The accordion believes a panel is being closed
                } else {
                    var currHeader  = ui.oldHeader;
                    var currContent = currHeader.next('.ui-accordion-content');
                }
                 // Since we've changed the default behavior, this detects the actual status
                var isPanelSelected = currHeader.attr('aria-selected') == 'true';
                
                 // Toggle the panel's header
                currHeader.toggleClass('ui-corner-all',isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top',!isPanelSelected).attr('aria-selected',((!isPanelSelected).toString()));
                
                // Toggle the panel's icon
                currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e',isPanelSelected).toggleClass('ui-icon-triangle-1-s',!isPanelSelected);
                
                 // Toggle the panel's content
                currContent.toggleClass('accordion-content-active',!isPanelSelected)    
                if (isPanelSelected) { currContent.slideUp(); }  else { currContent.slideDown(); }

                return false;
            }
        });
    });

    window.save_bundle_remove = function(e, i) {
        e.stopPropagation();
        jQuery('#ext-pro-bundle-item'+i).remove();
    }

    window.save_bundle_product = function(e) {
        e.stopPropagation();
    }
 
    // select 2 for product bundles
    jQuery('#render_all_products').select2({
        ajax: {
                url:  woopb_vars.ajax_url, 
                dataType: 'json',
                delay: 250, 
                data: function (params) {
                    return {
                        q: params.term, 
                        action: 'mishagetposts' 
                    };
                },
                
                processResults: function( data ) {
                var options = [];
                if ( data ) {
                    $.each( data, function( index, text ) { 
                        options.push( { id: text[0], text: text[1]  } );
                    });
                }
                return {
                    results: options
                };
            },
            cache: true
        },
        minimumInputLength: 3
    });


    // ajax call back getting products
    jQuery('#wooext_add_product_to_bundles').on('click', function (e) {
            
        var woopb_ids = jQuery('#render_all_products').val();
        var item_count = jQuery('#wooext_selectd_bundle .product-bundle-items').size();
        var pro_current_id = jQuery('#woopb_current_id').val();
        item_count++;
        if(woopb_ids ==null){
            jQuery('#wooext_add_product_to_bundles').val();
            jQuery('#render_all_products').children().remove();
            alert('Please add some product to save');
            return false;
        } else if(woopb_ids == pro_current_id){
            jQuery('#wooext_add_product_to_bundles').val();
            jQuery('#render_all_products').children().remove();
            alert('Child product cannot be same as bundled');
            return false;
        } else {
            jQuery(".woo-extloading").show();
            ajax: {
                data = {
                    action: 'get_bundle_products',
                    woopb_current_id : jQuery('#woopb_current_id').val(),
                    item_count : item_count,
                    woopb_ids : woopb_ids,
                    woopb_nonce: woopb_vars.woopb_nonce
                };
                jQuery.post(ajaxurl, data, function (response) {
                    jQuery('#wooext_add_product_to_bundles').val();
                    jQuery('#wooext_selectd_bundle').append(response).accordion('destroy').accordion({heightStyle: "content", collapsible : true, active : false, header: '> div.product-bundle-items >h3' });
                    jQuery('#render_all_products').children().remove();
                    jQuery(".woo-extloading").hide();
                });
            }
        }
    });


    jQuery( 'body' ).on( 'woocommerce-product-type-change', function ( event, select_val, select ) {
        if ( select_val == 'wooextb' ) {

            jQuery( 'input#_downloadable' ).prop( 'checked', false );
            jQuery( 'input#_virtual' ).removeAttr( 'checked' );

            jQuery( '.show_if_external' ).hide();
            jQuery( '.show_if_simple' ).show();
            jQuery( '.show_if_wooextb' ).show();

            jQuery( 'input#_downloadable' ).closest( '.show_if_simple' ).hide();
            jQuery( 'input#_virtual' ).closest( '.show_if_simple' ).hide();

            jQuery( 'input#_manage_stock' ).change();
            jQuery( 'input#_per_product_pricing_active' ).change();
            jQuery( 'input#_per_product_shipping_active' ).change();

            jQuery( '#_nyp' ).change();

            jQuery( '.pricing' ).show();
            jQuery( '.product_data_tabs' ).find( 'li.general_options' ).show();
        } else {
            jQuery( '.show_if_wooextb' ).hide();
        }

    } );

    // jQuery( 'select#product-type' ).change();

    // jQuery( '#_regular_price' ).closest( 'div.options_group' ).addClass( 'show_if_wooextb' );


    // -------------------------------------------------------
    // ----------------- Per item pricing ---------------------------
    // -------------------------------------------------------

    per_items_pricing = jQuery( '#_extbundle_per_item_pricing');

    per_items_pricing.on( 'change', function () {
        var on = $( this ).is( ':checked' );
        
        if ( on ) {
            // Per Item Pricing
            $( '#_regular_price' ).val( '' );
            $( '#_sale_price' ).val( '' );
            $( '.pricing' ).hide();
        } else {
            // NO -> Per sItem Pricing
            $( '.pricing' ).show();

            $( '.product_data_tabs' ).find( 'li.general_options' ).show();
        }
    });

});