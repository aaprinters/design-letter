<?php

if ( ! defined( 'ABSPATH' ) ) exit;  

class EXTENDONS_PRODUCT_BUNDLES_ADMIN extends EXTENDONS_PRODUCT_BUNDLES {
	
	public function __construct() {

		add_action( 'wp_loaded', array( $this, 'pro_bundles_enqueue_admin_scripts' ) );

		add_action( 'plugins_loaded', array($this,'wooext_product_bundles_product_type'));

		add_filter( 'woocommerce_product_data_tabs', array( $this, 'wooextb_product_data_tabs' ) );

		add_action( 'woocommerce_product_data_panels', array( $this, 'wooextb_product_data_panels' ) );

		add_action( 'woocommerce_process_product_meta_wooextb', array( $this, 'wooextb_save_option_field' ) );

		add_filter( 'product_type_selector', array( $this, 'wooextb_product_type_selector' ) );

		add_filter( 'product_type_options', array( $this, 'wooextb_product_type_options' ) );

		// add_action( 'woocommerce_process_product_meta_extmeasurement', array($this,'save_measurement_option_field'  ));

		// add_action('woocommerce_process_product_meta_variable', array($this, 'save_measurement_option_field'));

		// add_action('woocommerce_process_product_meta', array($this, 'save_measurement_option_field'));
		
	}

	public function wooext_product_bundles_product_type() {

		require_once( product_bundles_dir.'Include/woo-extendons-product-bundle-type.php' );
		
	}

	public function wooextb_product_data_tabs( $tabs ) {
		$tabs['wooextb'] = array(
			'label'  => esc_html__( 'Extendons Bundle', 'product-bundles-extendons' ),
			'target' => 'wooextb_settings',
			'class'  => array( 'show_if_wooextb' ),
		);

		return $tabs;
	}

	public function wooextb_product_type_selector( $types ) {
		
		$types['wooextb'] = _x( 'Extendons Bundle', 'product-bundles-extendons' );

		return $types;

	}

	public function wooextb_product_data_panels() {
			
		global $post;
		
		$post_id = $post->ID; ?>

		<div id='wooextb_settings' class='bootstrap-iso panel woocommerce_options_panel wooextb_table'>
			<div class="woo-extloading" style="display: none;"></div>
	
			<nav class="wooextb-nav navbar navbar-default">
                <button type="button" class="open-uiacc btn btn-primary btn-sm"><?php _e( 'Close all', 'product-bundles-extendons' ); ?></button>
                <button type="button" class="close-uiacc btn btn-primary btn-sm"><?php _e( 'Expand all', 'product-bundles-extendons' ); ?></button>
			</nav>

			<div id="wooext_selectd_bundle">
			  	
			  	<?php $bundle_data = get_post_meta($post_id, '_wcpb_bundles_product', true); 
			  	 		$bundle_data = unserialize($bundle_data);
			  	 		if ( !empty( $bundle_data ) ) {

	                    $i = 1;
	                    foreach ( $bundle_data as $item_data ) { 
	                    	$product = wc_get_product( $item_data['product_id'] ); ?>

	                    	<div class="product-bundle-items" id="ext-pro-bundle-item<?php echo $i; ?>">
							  	<h3>
							  		<strong>
							  			<a onclick="save_bundle_product(event);" target="_blank" href="<?php echo get_edit_post_link($product->get_id()); ?>" class="dashicons dashicons-welcome-view-site"></a>
							  			<span><?php echo $product->get_name().' - #'.$product->get_id(); ?></span>
							  		</strong>
							  		<button onclick="save_bundle_remove(event, '<?php echo $i; ?>');" type="button" class="save-product-bundles button"><?php _e( 'Remove', 'product-bundles-extendons' ); ?></button>
							  		<input type="hidden" value="<?php echo $product->get_id(); ?>" name="_wcpb_bundles_product[<?php echo $i; ?>][product_id]" />
							  		<input type="hidden" value="<?php echo $i; ?>" name="_wcpb_bundles_product[<?php echo $i; ?>][product_item_id]">
							  		<input type="hidden" value="<?php echo $item_data['product_price']; ?>" name="_wcpb_bundles_product[<?php echo $i; ?>][product_price]" />
							  	</h3>
							  	<div>
							    	
							    	<div class="bundles-form">
							    		<div class="col-md-8">
											<div class="form-group">
												<label class="search-wooextb-labelfld" for=""><?php _e( 'Group Name', 'product-bundles-extendons' ); ?></label>
												<select name="_wcpb_bundles_product[<?php echo $i; ?>][group_name]">
												<?php if($item_data['group_name']){ ?>
													<option value="<?php echo $item_data['group_name']; ?>"><?php echo str_replace('_', ' ', $item_data['group_name']);; ?></option>
												<?php
												}else{
													?>
													<option value="">Select Group</option>
													<?php
												}
												  ?>
												  <option value="letter">Letter</option>
												  <option value="chain_length">Chain length</option>
												  <option value="icon_charm">Icon Charm</option>
												  <option value="stone_charm">Semi-Precious Stone Charm</option>
												  <option value="enamel_charm">Enamel Charm</option>
												  <option value="stone_heart_charm">Stone Heart Charm</option>
												  <option value="ball_charm">Ball Charm</option>
												  <option value="opal_charm">Opal Charm</option>
												  <option value="charm_gold">Charm Gold</option>
												  <option value="gift_box">Gift Box</option>
												  <option value="stickers_statements">Stickers Statements</option>
												</select>
											</div>
											<div class="form-group">
												<label class="search-wooextb-labelfld" for=""><?php _e( 'Hide Product', 'product-bundles-extendons' ); ?></label>
												<input onChange="echeckbox(this);" <?php if(isset($item_data['hide_product']) && '1' == $item_data['hide_product'] ) echo 'checked="checked"'; ?> value="1" name="_wcpb_bundles_product[<?php echo $i; ?>][hide_product]" type="checkbox" />
												<span class="woocommerce-help-tip" data-tip="<?php _e( 'Check if you want to hide this product in bundle', 'product-bundles-extendons' ) ?>"></span>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label class="search-wooextb-labelfld" for=""><?php _e( 'Hide Thumbnail', 'product-bundles-extendons' ); ?></label>
												<input onChange="echeckbox(this);" <?php if(isset($item_data['hide_thumbnail']) && '1' == $item_data['hide_thumbnail'] ) echo 'checked="checked"'; ?> name="_wcpb_bundles_product[<?php echo $i; ?>][hide_thumbnail]" type="checkbox" value="1"/>
												<span class="woocommerce-help-tip" data-tip="<?php _e( 'Check if you want to hide thumbnail of this product in bundle', 'product-bundles-extendons' ) ?>"></span>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label class="search-wooextb-labelfld" for=""><?php _e( 'Optional', 'product-bundles-extendons' ); ?></label>
												<input onChange="echeckbox(this);" <?php if(isset($item_data['optional_product']) && '1' == $item_data['optional_product'] ) echo 'checked="checked"'; ?> name="_wcpb_bundles_product[<?php echo $i; ?>][optional_product]" type="checkbox" value="1"/>
												<span class="woocommerce-help-tip" data-tip="<?php _e( 'Check if you want to make optional this product in bundle', 'product-bundles-extendons' ) ?>"></span>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label class="search-wooextb-labelfld" for=""><?php _e( 'Quantity', 'product-bundles-extendons' ); ?></label>
												<input value="<?php echo $item_data['product_quantity']; ?>" name="_wcpb_bundles_product[<?php echo $i; ?>][product_quantity]" type="number" class="form-control" min="1"/>
												<span class="woocommerce-help-tip" data-tip="<?php _e( 'Set quantity of this product', 'product-bundles-extendons' ) ?>"></span>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label class="search-wooextb-labelfld" for=""><?php  _e( 'Max Quantity Allowed', 'product-bundles-extendons' ); ?></label>
												<input value="<?php echo $item_data['max_product_quantity']; ?>" name="_wcpb_bundles_product[<?php echo $i; ?>][max_product_quantity]" type="number" class="form-control" min="1"/>
												<span class="woocommerce-help-tip" data-tip="<?php _e( 'Enter maximum quantity allowed for this product', '' ); ?>"></span>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label class="search-wooextb-labelfld" for=""><?php _e( 'Product Title', 'product-bundles-extendons' ); ?></label>
												<input value="<?php echo $item_data['product_title']; ?>" name="_wcpb_bundles_product[<?php echo $i; ?>][product_title]" type="text" class="form-control" />
												<span class="woocommerce-help-tip" data-tip="<?php _e( 'Put Title of this product', 'product-bundles-extendons' ) ?>"></span>
											</div>
											<div class="clearfix"></div>
											<div class="form-group">
												<label class="search-wooextb-labelfld" for=""><?php _e( 'Product Description', 'product-bundles-extendons' ); ?></label>
												<textarea name="_wcpb_bundles_product[<?php echo $i; ?>][product_description]" class="form-control" rows="6" col="4"><?php echo $item_data['product_description']; ?></textarea>
												<span class="woocommerce-help-tip" data-tip="<?php _e( 'Put Description of this product', 'product-bundles-extendons' ) ?>"></span>
											</div>
										</div>
							    	</div>

							  	</div>
						  	</div>

                <?php $i++; } } ?>

			</div>
			<script type="text/javascript">
				function echeckbox(t) {
			        jQuery(t).val(t.checked ? "1" : "0");
			    }
			</script>

			<!-- search form -->
			<div class="row">
				
				<div class="col-md-10 search-product-col">
					<div class="row">
						<div class="col-md-8">

							<div class="form-group">
								<label class="search-wooextb-label" for="wooext_keyword"><?php esc_html_e('Select Products', 'product-bundles-extendons'); ?></label>
								<select class="form-control" id="render_all_products" name="render_all_products[]"></select>
							</div>

						</div>
						<div class="col-md-2">
							<button type="button" id="wooext_add_product_to_bundles" class="btn btn-primary btn-sm"><?php _e('Add Product', 'product-bundles-extendons'); ?></button>
							<input type="hidden" id="woopb_current_id" value="<?php echo $post_id; ?>">
						</div>
					</div>
				</div>

			</div>

		</div>
		
		<?php
	}

	public function wooextb_save_option_field($post_id) {

		$product = wc_get_product( $post_id );
        
        if ( !$product )
           	return;

        $wcpb_bundle_data = isset( $_POST[ '_wcpb_bundles_product' ] ) ? $_POST[ '_wcpb_bundles_product' ] : false;
       
        update_post_meta($post_id, '_wcpb_bundles_product', serialize($wcpb_bundle_data));

        // // per item pricing
        $bundle_per_items_price = isset( $_POST[ '_extbundle_per_item_pricing' ] ) ? 'yes' : 'no';
        update_post_meta( $post_id, '_extbundle_per_item_pricing', $bundle_per_items_price );

	}

	 public function wooextb_product_type_options( $options ) {

        $options[ 'extbundle_per_item_pricing' ] = array(
            'id'            => '_extbundle_per_item_pricing',
            'wrapper_class' => 'show_if_wooextb',
            'label'         => __( 'Per Item Pricing', 'product-bundles-extendons' ),
            'description'   => __( 'Check this option if you want your bundle priced per item, that is based on item prices and tax rates.', 'product-bundles-extendons' ),
            'default'       => 'no'
        );

        return $options;
    }

	public function pro_bundles_enqueue_admin_scripts() {

		wp_enqueue_script('jquery');

		wp_enqueue_style( 'pro-bundles-backend-style', plugins_url( '/Styles/backend.css', __FILE__), false);

		wp_enqueue_script( 'product-bundles-backend.js', plugins_url( '/Scripts/back-end.js',__FILE__ ), false);

		wp_enqueue_style( 'pro-bundles-select-style', plugins_url( '/Styles/select2.min.css', __FILE__), false);

		wp_enqueue_script( 'product-bundles-select.js', plugins_url( '/Scripts/select2.min.js',__FILE__ ), false);

		wp_localize_script( 'product-bundles-backend.js', 'woopb_vars', array(
				'woopb_nonce' => wp_create_nonce( 'woopb_nonce' ),
				'ajax_url' => admin_url( 'admin-ajax.php' )
			)
		);
		
	}

} new EXTENDONS_PRODUCT_BUNDLES_ADMIN();





