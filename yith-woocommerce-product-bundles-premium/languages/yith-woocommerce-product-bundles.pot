#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: YITH WooCommerce Product Bundles\n"
"POT-Creation-Date: 2018-09-28 14:39+0200\n"
"PO-Revision-Date: 2015-06-03 14:00+0100\n"
"Last-Translator: \n"
"Language-Team: Yithemes <plugins@yithemes.com>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.1.1\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;"
"__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;"
"_nx_noop:4c,1,2\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=n!=1;\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-SearchPathExcluded-0: ../plugin-fw\n"

#: ../class.yith-wcpb-admin-premium.php:119 ../class.yith-wcpb-admin.php:256
msgid "Bundled Items"
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:125
msgid "Bundle Options"
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:254
msgid "Per Item Pricing"
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:255
msgid ""
"Check this option if you want your bundle priced per item, that is based on "
"item prices and tax rates."
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:262
msgid "Non-Bundled Shipping"
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:263
msgid ""
"Check this option if you would like that the bundle items will be shipped "
"individually."
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:308
msgctxt "Admin: filter variations of the bundled product if it is variable."
msgid "Filter Product Variations"
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:310
msgid "Choose variations&hellip;"
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:328
msgid "Any"
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:378
msgid ""
"Select the variations allowed for this item. To allow all variations leave "
"it empty."
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:381
msgctxt ""
"Admin: overwrite the default selection for attributes of variable product."
msgid "Overwrite default selection"
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:388
msgid "No default"
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:415
msgid ""
"Overwrite the default selection of the attribute for the variable product."
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:419
msgctxt "Admin: hide item of the bundled product."
msgid "Hide Product"
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:422
msgid "Check this option if you would like to hide this product in the bundle."
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:425
msgctxt "Admin: hide the thumbnail of the bundled product."
msgid "Hide thumbnail"
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:428
msgid ""
"Check this option if you would like to hide the thumbnail of this product."
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:431
msgctxt "Admin: mark the bundled product as optional."
msgid "Optional"
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:434
msgid "Check this option if you would like to mark this product as optional."
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:437
msgctxt "Admin: minimum quantity of the bundled product."
msgid "Min quantity"
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:440
msgid "Choose the minimum quantity for this product."
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:443
msgctxt "Admin: maximum quantity of the bundled product."
msgid "Max quantity"
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:446
msgid "Choose the maximum quantity for this product."
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:449
msgctxt "Admin: discount for the bundled product."
msgid "Discount %"
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:452
msgid ""
"Choose the discount for this product. If a discount is applied on a bundled "
"product on sale, its price will be the regular one discounted by the chosen "
"percentage."
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:455
msgctxt "Admin: the title of the bundled product."
msgid "Title"
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:457
msgid "Choose the title for this product."
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:460
msgctxt "Admin: the description of the bundled product."
msgid "Description"
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:463
msgid "Choose the description for this product."
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:489 ../class.yith-wcpb-admin.php:412
msgid "You cannot add a bundle product"
msgstr ""

#: ../class.yith-wcpb-admin-premium.php:510
msgid "Settings"
msgstr ""

#: ../class.yith-wcpb-admin.php:170
#, php-format
msgid "%1$s item [%2$s bundled elements]"
msgid_plural "%1$s items [%2$s bundled elements]"
msgstr[0] ""
msgstr[1] ""

#: ../class.yith-wcpb-admin.php:184
msgctxt "Admin: type of product"
msgid "Product Bundle"
msgstr ""

#: ../class.yith-wcpb-admin.php:201
msgctxt "Admin: quantity of the bundled product."
msgid "Quantity"
msgstr ""

#: ../class.yith-wcpb-admin.php:238 ../class.yith-wcpb-admin.php:411
msgid ""
"You can add only simple products with the FREE version of YITH WooCommerce "
"Product Bundles"
msgstr ""

#: ../class.yith-wcpb-admin.php:366
msgid "How to"
msgstr ""

#: ../class.yith-wcpb-admin.php:367
msgid "Premium Version"
msgstr ""

#: ../class.yith-wcpb-frontend-premium.php:902
#, php-format
msgid "The minimum number of &quot;%1$s&quot; required is %2$s"
msgstr ""

#: ../class.yith-wcpb-frontend-premium.php:908
#, php-format
msgid "The maximum number of &quot;%1$s&quot; allowed is %2$s"
msgstr ""

#: ../class.yith-wcpb-frontend-premium.php:928
#, php-format
msgid ""
"&quot;%1$s&quot; cannot be added to the cart. The selected variation of "
"&quot;%2$s&quot; cannot be purchased."
msgstr ""

#: ../class.yith-wcpb-frontend-premium.php:936
#: ../class.yith-wcpb-frontend-premium.php:956
#, php-format
msgid ""
"&quot;%1$s&quot; cannot be added to the cart because &quot;%2$s&quot; cannot "
"be purchased at the moment."
msgstr ""

#: ../class.yith-wcpb-frontend-premium.php:942
#: ../class.yith-wcpb-frontend-premium.php:961
#: ../class.yith-wcpb-frontend.php:438
msgid ""
"You cannot add this quantity of items, because there are not enough in stock."
msgstr ""

#: ../class.yith-wcpb-frontend-premium.php:948
#, php-format
msgid ""
"&quot;%1$s&quot; cannot be added to the cart. Please choose an option for "
"&quot;%2$s&quot;&hellip;"
msgstr ""

#: ../class.yith-wcpb-frontend-premium.php:992
#, php-format
msgid ""
"The minimum number of items in the bundle &quot;%1$s&quot; required is %2$s"
msgstr ""

#: ../class.yith-wcpb-frontend-premium.php:1000
#, php-format
msgid ""
"The maximum number of items in the bundle &quot;%1$s&quot; allowed is %2$s"
msgstr ""

#: ../functions.yith-wcpb.php:21
msgid "Simple"
msgstr ""

#: ../functions.yith-wcpb.php:22
msgid "Variable"
msgstr ""

#: ../includes/class.yith-wc-product-bundle-premium.php:318
#: ../includes/class.yith-wc-product-bundle.php:166
msgid "Add to cart"
msgstr ""

#: ../includes/class.yith-wc-product-bundle-premium.php:318
#: ../includes/class.yith-wc-product-bundle.php:166
msgid "Read more"
msgstr ""

#: ../includes/class.yith-wc-product-bundle-premium.php:501
#, php-format
msgctxt "From price"
msgid "From %s"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:28
msgid "Display a list of your product bundles on your site."
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:30
msgid "YITH WooCommerce Product Bundle"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:35
msgid "Product Bundles"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:36
msgid "Title"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:44
msgid "Number of products to show"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:49
msgid "Show"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:51
msgid "All Products"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:52
msgid "Featured Products"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:53
msgid "On Sale Products"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:59
msgid "Order by"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:61
msgid "Date"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:62
#: ../templates/admin/select-product-box-products.php:31
msgid "Price"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:63
msgid "Random"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:64
msgid "Sales"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:70
msgctxt "Sorting order"
msgid "Order"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:72
msgid "ASC"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:73
msgid "DESC"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:79
msgid "Hide free bundles"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:84
msgid "Show hidden bundles"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:89
msgid "Show products in bundle"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:94
msgid "Show thumbnails for products in bundle"
msgstr ""

#: ../includes/class.yith-wcpb-bundle-widget.php:99
msgid "Show only bundles including the currently viewed product"
msgstr ""

#: ../init.php:52
msgid ""
"YITH WooCommerce Product Bundles Premium is enabled but not effective. It "
"requires WooCommerce in order to work."
msgstr ""

#: ../plugin-options/settings-options.php:14
msgid "General Options"
msgstr ""

#: ../plugin-options/settings-options.php:22
msgid "Show bundled items in Reports"
msgstr ""

#: ../plugin-options/settings-options.php:24
msgid "Flag this option to show also the bundled items in WooCommerce Reports."
msgstr ""

#: ../plugin-options/settings-options.php:30
msgid "Hide bundled items in Cart and Checkout"
msgstr ""

#: ../plugin-options/settings-options.php:32
msgid ""
"Flag this option to hide the bundled items in WooCommerce Cart and Checkout."
msgstr ""

#: ../plugin-options/settings-options.php:38
msgid "Out of stock Sync"
msgstr ""

#: ../plugin-options/settings-options.php:40
msgid ""
"Flag this option to set the bundle as Out of Stock if it contains at least "
"one Out of Stock item."
msgstr ""

#: ../plugin-options/settings-options.php:46
msgid "Price of \"per item pricing\" bundles in Shop"
msgstr ""

#: ../plugin-options/settings-options.php:51
msgid "Min - Max"
msgstr ""

#: ../plugin-options/settings-options.php:55
msgid "Min only"
msgstr ""

#: ../plugin-options/settings-options.php:59
msgid "Min only higher than"
msgstr ""

#: ../plugin-options/settings-options.php:63
msgid "Regular and discounted"
msgstr ""

#: ../plugin-options/settings-options.php:67
msgid ""
"Choose how you want to view pricing for \"per item pricing\" bundle products"
msgstr ""

#: ../plugin-options/settings-options.php:73
msgid "Price of \"per item pricing\" bundles in orders"
msgstr ""

#: ../plugin-options/settings-options.php:76
msgid "Price in bundle"
msgstr ""

#: ../plugin-options/settings-options.php:77
msgid "Price in bundled items"
msgstr ""

#: ../plugin-options/settings-options.php:79
msgid ""
"Choose how you want to view order pricing for \"per item pricing\" bundle "
"products"
msgstr ""

#: ../plugin-options/settings-options.php:85
msgid "Quick View for bundled items"
msgstr ""

#: ../plugin-options/settings-options.php:87
msgid "If enabled, open bundled item product link in Quick View."
msgstr ""

#: ../plugin-options/settings-options.php:94
msgid "Bundle price sync"
msgstr ""

#: ../plugin-options/settings-options.php:98
msgid "Force price sync for \"per item pricing\" bundles"
msgstr ""

#: ../templates/admin/admin-bundle-options-tab.php:23
#: ../templates/premium/admin/admin-bundle-options-tab.php:32
msgid "Close all"
msgstr ""

#: ../templates/admin/admin-bundle-options-tab.php:24
#: ../templates/premium/admin/admin-bundle-options-tab.php:33
msgid "Expand all"
msgstr ""

#: ../templates/admin/admin-bundle-options-tab.php:50
#: ../templates/premium/admin/admin-bundle-options-tab.php:58
msgid "Add Product"
msgstr ""

#: ../templates/admin/admin-bundled-product-item.php:34
#: ../templates/premium/admin/admin-bundled-product-item.php:32
msgid "Remove"
msgstr ""

#: ../templates/admin/admin-bundled-product-item.php:35
#: ../templates/premium/admin/admin-bundled-product-item.php:33
msgid "Click to toggle"
msgstr ""

#: ../templates/admin/admin-bundled-product-item.php:39
#: ../templates/premium/admin/admin-bundled-product-item.php:37
msgid "Not Purchasable"
msgstr ""

#: ../templates/admin/select-product-box-products.php:29
msgid "Image"
msgstr ""

#: ../templates/admin/select-product-box-products.php:30
msgid "Name"
msgstr ""

#: ../templates/admin/select-product-box-products.php:32
msgid "Type"
msgstr ""

#: ../templates/admin/select-product-box-products.php:33
msgid "Action"
msgstr ""

#: ../templates/admin/select-product-box-products.php:52
#: ../templates/single-product/add-to-cart/yith-bundle.php:57
msgid "Out of stock"
msgstr ""

#: ../templates/admin/select-product-box-products.php:59
#: ../templates/premium/single-product/add-to-cart/yith-bundle-items-list.php:174
#: ../templates/premium/single-product/add-to-cart/yith-bundle-items-list.php:203
msgid "Add"
msgstr ""

#: ../templates/admin/select-product-box-products.php:60
msgid "Added"
msgstr ""

#: ../templates/admin/select-product-box-products.php:74
msgid "prev"
msgstr ""

#: ../templates/admin/select-product-box-products.php:76
msgid "next"
msgstr ""

#: ../templates/admin/select-product-box.php:8
msgid "Search for a product (min 3 characters)"
msgstr ""

#: ../templates/premium/admin/admin-bundle-options-tab.php:70
msgid "Minimum number of items in bundle"
msgstr ""

#: ../templates/premium/admin/admin-bundle-options-tab.php:78
msgid ""
"Minimum number of items/products that customers have to pick in order to be "
"able to add the bundle to the cart"
msgstr ""

#: ../templates/premium/admin/admin-bundle-options-tab.php:82
msgid "Maximum number of items in bundle"
msgstr ""

#: ../templates/premium/admin/admin-bundle-options-tab.php:90
msgid ""
"Maximum number of items/products that customers can pick in order to be able "
"to add the bundle to the cart"
msgstr ""

#: ../templates/premium/compatibility/request-a-quote/raq-table-row-email.php:32
#: ../templates/premium/compatibility/request-a-quote/raq-table-row.php:33
msgid " SKU:"
msgstr ""

#: ../templates/premium/single-product/add-to-cart/yith-bundle-items-list.php:176
#, php-format
msgid "Add for %s"
msgstr ""

#: ../templates/premium/single-product/add-to-cart/yith-bundle-items-list.php:222
msgid "Choose an option"
msgstr ""

#: ../templates/single-product/add-to-cart/yith-bundle.php:55
msgid "In stock"
msgstr ""
